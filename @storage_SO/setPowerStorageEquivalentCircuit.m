%% setPowerStorage
%   Method to set the power power of the storage system.
%
%   Method sets the method of the storage system within allowed limits.
%   Rated power of inverter is considered. Invoked functions also check the
%   SOC-limits and limit the powerStorage for the system to remain within
%   allowed operation limits.
%   Efficiency losses of the power electronics and the battery is included.
%   Proposed method to control the energy storage system.
%
%   2017-08-03 Truong
%
%%


function [ EES ] = setPowerStorageEquivalentCircuit( EES, powerStorageNow )
% object vars --> local vars (execution speed)
stepNow                     = EES.stepNow;
sampleTime                  = EES.sampleTimeNow;
etaAccuracy                 = EES.inputTech.etaAccuracy;
powerElectronicsRatedPower  = EES.inputTech.powerElectronicsRatedPower;
temperatureAmbient          = EES.inputTech.temperatureAmbient;
ecModelParams               = EES.inputTech.batteryModelParameters;
sohCapacityNow              = EES.SOHCapacityNow;
sohResistanceNow            = EES.SOHResistanceNow;
battSelfDischRate           = EES.inputTech.selfDischargeRate;
SOCnow                      = EES.SOCNow;

ecModelParams.Cell.SOC_Min = EES.SOCLimLow;
ecModelParams.Cell.SOC_Max = EES.SOCLimHigh;

% add system standbyLoad to inverter power
stbyLoad        = 0; % dummy of 0 watts --> model required
powerInverter   = powerStorageNow - stbyLoad;
% keep powerStorage within device limits and compute power at battery terminal
powerInverter   = max(powerInverter, -powerElectronicsRatedPower);   % limit discharging power to max. PowerElectronics power
powerInverter   = min(powerInverter,  powerElectronicsRatedPower);   % limit charging power to max. PowerElectronics power
idxEta          = ceil( etaAccuracy * ( powerInverter/powerElectronicsRatedPower + 1 ) + 1 );
etaPENow        = EES.inputTech.etaPowerElectronics( idxEta );
powerBattT      = etaPENow * powerInverter; 

% update of battery temperature
if stepNow == 1
    temperatureBattNow = temperatureAmbient;   % in case 
else
    temperatureBattNow = EES.temperature(stepNow-1) ;
end

% call of EC function: computes SOC via EC
[   powerBattNow, ... 
    powerLossBattNow, ...
    ocvBattNow, ...
    voltageBattNow, ...
    currentBattNow, ...
    newSOC, ...
    etaBattNow, ...
    riNow] = battModel_EC_OCV_R( powerBattT, ...
                        sampleTime, ...
                        SOCnow, ...
                        sohCapacityNow, ...
                        sohResistanceNow, ...
                        temperatureBattNow, ...
                        ecModelParams, ...
                        battSelfDischRate);

% check for limits of battery or power electronics
powerBattNow    = min( powerBattNow,  powerElectronicsRatedPower );
powerBattNow    = max( powerBattNow, -powerElectronicsRatedPower );
% if power is adjusted to limit, recalculate pAc of inverter
if powerBattT ~= powerBattNow
    etaIdx          = ceil( etaAccuracy * ( powerBattNow/powerElectronicsRatedPower +1 ) + 1 );
    etaPENow        = EES.inputTech.etaPEInverse( etaIdx );
    powerInverter   = etaPENow * powerBattNow;
end
% calculate the output of the storage system (inverter - stby)
powerStorageNow     = powerInverter + stbyLoad;

% compute battery temperature
[ EES.temperature(stepNow) ] = battModel_thermalCell( powerLossBattNow, ecModelParams, temperatureBattNow,  temperatureAmbient, sampleTime );

% update of object properties
EES.SOCNow                  = newSOC;
EES.SOC(stepNow)            = newSOC;                   % update SOC trend
EES.powerStorageNow         = powerStorageNow;
EES.powerStorage(stepNow)   = powerStorageNow;          % set powerStoragenow
EES.powerBattNow            = powerBattNow;  
EES.powerBatt(stepNow)      = powerBattNow;             % set trend of power at battery terminal

% Logging if flag for logging of battery EC output values is activated
if(EES.inputSim.logBatteryEcOutput)
    EES.cellStates.powerLoss(stepNow) = powerLossBattNow;
    EES.cellStates.voltage(stepNow)   = voltageBattNow;
    EES.cellStates.current(stepNow)   = currentBattNow;
    EES.cellStates.eta(stepNow)       = etaBattNow;
    EES.cellStates.ocv(stepNow)       = ocvBattNow;
    EES.cellStates.ri(stepNow)        = riNow;
end

% compute battery stress and resulting aging
characterizeStress( EES );
calcAging( EES );

end % end of function

