%% calcAging
% 
% Sets battery's SOH values of capacity and resistance calculated by the 
% aging model. Needs to be called after the characterizeStress method is 
% called.
%
% Input/Output: EES object
%
% This functions sets battery's SOH values of capacity and resistance,
% which are calculated by calling the aging models wihtin the function 
% callMethodAgingModels. There are different selectable options in form of
% functions handels for calling the aging models:
%   - callMethodAgingModels_SingleValues
%   - callMethodAgingModels_AverageValues
% Calendric, cyclic and total aging values are logged seperatly in the EES 
% object depending on the selected logging option. The aging stress values,
% which were used for the aging models, are logged inside the EES object.
% This functions needs to be called after the characterizeStress method is 
% called, to have all necessary input for the aging models.
%
% 2017-08-04   Maik Naumann
%
%%

function EES = calcAging( EES )

%% Assing input parameters
stepNow                 = EES.stepNow;
SOHCapacityNow          = EES.SOHCapacityNow;
SOHResistanceNow        = EES.SOHResistanceNow;
storNominalCapacity     = EES.inputTech.batteryNominalEnergy;
storNominalResistance   = 1;
            
% If logging is activated, current step is used for loggingIndex)
if(EES.inputSim.logAgingResults)                                            
    idxLogging          = stepNow;
else
    idxLogging          = 1;
end

%% Calculate aging according to selected calling method
[detectedStress, agingCalendric, agingCyclic, agingTotal] = EES.inputTech.callMethodAgingModels(EES);  

%% Logging of detected stress
EES.agingStress.cumAgingTime                = detectedStress.cumAgingTime;
EES.agingStress.cumRelCapacityThroughput    = detectedStress.cumRelCapacityThroughput;

% If logging is activated, all detected stress is logged
if(EES.inputSim.logAgingResults)  
    % Update EES aging stress values
    EES.agingStress.lastCycle(detectedStress.idxLogging)   = detectedStress.lastCycle;
    EES.agingStress.minSOC(detectedStress.idxLogging)      = detectedStress.minSOC;
    EES.agingStress.maxSOC(detectedStress.idxLogging)      = detectedStress.maxSOC;
    EES.agingStress.avgCRate(detectedStress.idxLogging)    = detectedStress.avgCRate;
    EES.agingStress.avgSOC(detectedStress.idxLogging)      = detectedStress.avgSOC;
end

%% Calculate remaining capacity with aging factors
SOHCapacityNow        = max(SOHCapacityNow      + agingTotal.relCapacityChange,     0);
SOHResistanceNow      = max(SOHResistanceNow    - agingTotal.relResistanceChange,   0);

%% Update EES values
% Aging values needed for aging models
EES.totalRelCapacityChangeCalendricNow     = EES.totalRelCapacityChangeCalendricNow   + agingCalendric.relCapacityChange;
EES.totalRelResistanceChangeCalendricNow   = EES.totalRelResistanceChangeCalendricNow + agingCalendric.relResistanceChange;
EES.totalRelCapacityChangeCyclicNow        = EES.totalRelCapacityChangeCyclicNow      + agingCyclic.relCapacityChange;
EES.totalRelResistanceChangeCyclicNow      = EES.totalRelResistanceChangeCyclicNow    + agingCyclic.relResistanceChange;
EES.totalRelCapacityChangeNow              = EES.totalRelCapacityChangeNow            + agingTotal.relCapacityChange;
EES.totalRelResistanceChangeNow            = EES.totalRelResistanceChangeNow          + agingTotal.relResistanceChange;

% If logging is activated, current value changes are logged. 
if(EES.inputSim.logAgingResults) 
    EES.capacityChangeCalendric(idxLogging)     = agingCalendric.relCapacityChange * storNominalCapacity;
    EES.capacityChangeCyclic(idxLogging)        = agingCyclic.relCapacityChange * storNominalCapacity;
    EES.capacityChangeTotal(idxLogging)         = agingTotal.relCapacityChange * storNominalCapacity;
    EES.resistanceChangeCalendric(idxLogging)   = agingCalendric.relResistanceChange * storNominalResistance;
    EES.resistanceChangeCyclic(idxLogging)      = agingCyclic.relResistanceChange * storNominalResistance;
    EES.resistanceChangeTotal(idxLogging)       = agingTotal.relResistanceChange * storNominalResistance;
% If logging is deactivated, values are summed up
else
    EES.capacityChangeCalendric     = EES.capacityChangeCalendric   + agingCalendric.relCapacityChange * storNominalCapacity;
    EES.capacityChangeCyclic        = EES.capacityChangeCyclic      + agingCyclic.relCapacityChange * storNominalCapacity;
    EES.capacityChangeTotal         = EES.capacityChangeTotal       + agingTotal.relCapacityChange * storNominalCapacity;
    EES.resistanceChangeCalendric   = EES.resistanceChangeCalendric + agingCalendric.relResistanceChange * storNominalResistance;
    EES.resistanceChangeCyclic      = EES.resistanceChangeCyclic    + agingCyclic.relResistanceChange * storNominalResistance;
    EES.resistanceChangeTotal       = EES.resistanceChangeTotal     + agingTotal.relResistanceChange * storNominalResistance;
end

EES.SOHCapacityNow          = SOHCapacityNow;
EES.SOHResistanceNow        = SOHResistanceNow;
EES.SOHCapacity(stepNow)    = SOHCapacityNow;
EES.SOHResistance(stepNow)  = SOHResistanceNow;

end