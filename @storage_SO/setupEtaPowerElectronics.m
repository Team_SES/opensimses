%% createEtaPowerElectronics
%
% Sub-Creator method to obtain efficiency vector for power eletronics
% during object generation.
% Creates array struct to assign powerStorage to according powerBatt.
% 
%
%   Creates array struct to assign powerStorage to according powerBatt
%   after inverter efficiency losses. 
%   Following assumption: P_out = eta * P_in.
%   Usage if efficiency is however always P_batt = eta * P_storage
%   Efficiency needs to be inversed for negative power (discharge case) to
%   correctly calculate powers.
%   Function is called during parameterization at the first simulation
%   step.
%
% 2017-07-27 Nam Truong
%
%%



function [ EES ] = setupEtaPowerElectronics( EES )
etaInput        = EES.inputTech.powerElectronicsEta;
PERatedPower    = EES.inputTech.powerElectronicsRatedPower;
% eta curve = f(p)
if length(etaInput) > 1
    % construct etaPE = f(pAc)
    etaIn_pAc   = linspace( 0, PERatedPower, length(etaInput) );    % get x-axis for given eta-Input (assume that etaInput is evenly distributed)
    x           = linspace( 0, PERatedPower, EES.inputTech.etaAccuracy + 1 ); % creates x with length of EES.etaAccuracy + 1 (for 0 value)
    negEta      = interp1( etaIn_pAc, 1 ./ etaInput, x );   % create interpolated efficiency curve for negative values
    negEta      = negEta( end:-1:2 );                       % reverse order of curve for correct total efficiency curve
    posEta      = interp1( etaIn_pAc, etaInput, x );        % interpolate to given fidelity (etaAccuracy)
    etaPE       = [ negEta, posEta ];                       % concat negative and positive efficiency curves

    % construct etaInv = f(pDc) for correction-mechanism in setPower-Method
    pAcIn   = [-x(end:-1:2), x];   
    pDcIn   = pAcIn.*etaPE;
    % limit pDc during discharge to PERatedPower
    pDcT    = min( pDcIn,  PERatedPower);
    pDcT    = max( pDcT,  -PERatedPower);
    % create even space of input pDc
    pDc     = linspace( pDcT(1) , pDcT(end) , length(pDcIn) );
    pAc     = interp1( pDcIn, pAcIn, pDc );
    etaInv  = pAc ./ pDc;
 
% constant eta without power dependency
else 
    negEta  = repmat(1./etaInput, 1, EES.inputTech.etaAccuracy);
    posEta  = repmat(etaInput, 1, EES.inputTech.etaAccuracy + 1);
    etaPE   = [negEta, posEta];
    etaInv  = fliplr(etaPE);
end

% write into object
EES.inputTech.etaPowerElectronics   = etaPE(:); 
EES.inputTech.etaPEInverse          = etaInv(:); 

end

