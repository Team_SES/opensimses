%% Class definition: storage_SO
% Object type: Matlab system object
% Class definition simulation of storage. The model includes operational
% limits (SOC, power), losses and aging.
%
% Call via name-value pair
% ees = storage('inputSim', inputSim, 'inputTech', inputTech, 'economicData', economicData)
%
% Output:
% - ees: object for simulation
%
% Input:
% - inputSim: struct with simulation parameters in respective struct-fields
% - inputTech: struct with technical parameters of the storage system 
%   and its aux. components.
% - economicData: struct with economic input data of storage system
%
% Class definition for simulation of storage system. Can be implemented in
% matlab script or simulink model, with respective input data.
% 
% Operational limits (SOC and power) are checked within setPower method and
% do not require consideration in the control algorithm.
% Object states are updated after reference power is given. Submodels for
% aging, aux. components, battery are used for computation of state
% changes.
%
% 2017-08-04 Naumann/Truong



classdef storage_SO < matlab.System & matlab.system.mixin.Propagates & matlab.system.mixin.CustomIcon
    properties (Nontunable, Access = public)        
        inputSim        = @(x) [];  % Input parameter struct for simulation configuration of storage object
        inputTech       = @(x) [];  % Input parameter struct for technical configuration of storage object
        setPowerStorage = @(x) [];  % Function handle to determine how SOC etc. is handled (EC or power flow) 
    end

    %%% momentary states: When adding new states name has to end with 'Now'
    %%% for proper object saving and loading
    properties(DiscreteState)
        powerStorageNow             % [W] Current visible power of ES at inverter terminal (connection to grid) (charging positive, discharging negative)         
        powerBattNow                % [W] Current power seen by battery at terminal / DC-side of inverter
        powerStorageOpNow           % [W] Current power consumption required to operate storage device (sensors, controller, thermal management)
        SOCNow                      % [pu] current SOC
        SOHCapacityNow              % [pu] Current SOH of storage capacity
        SOHResistanceNow            % [pu] Current SOH of storage resistance
        
        totalRelCapacityChangeCalendricNow  % [Ws] capacity change due to calendric aging
        totalRelCapacityChangeCyclicNow     % [Ws] capacity change due to cycle aging
        totalRelCapacityChangeNow           % [Ws] capacity change due to calendric and cycle aging
        totalRelResistanceChangeCalendricNow    % [Ohm] resistance change due to calendric aging
        totalRelResistanceChangeCyclicNow       % [Ohm] resistance change due to cycle aging
        totalRelResistanceChangeNow             % [Ohm] resistance change due to calendric and cycle aging

        timeNow                     % [s] Time since start of simulation
        stepNow                     % [~] Steps since start of simulation

        sampleTimeNow               % [s] sample time of current step
        lastStateVectorUpdateTimeNow   % [s] time of last state vector update 
    end
    
    
    %%% arrays to save history of states etc.
    properties(GetAccess = public, SetAccess = private, Hidden = false)
        % Battery states
        powerStorage                % [W] visible power of ES at inverter terminal (connection to grid) (charging positive, discharging negative) 
        powerBatt                   % [W] power seen by battery at terminal
        SOC                         % [pu] state of charge
        SOCLimHigh                  % [pu] upper SOC limit --> tunable for some EMS
        SOCLimLow                   % [pu] lower SOC limit
        cellStates                  % [-] Struct for logging of output values of battery EC model
        temperature                 % [K] Battery temperature

        % Aging trends
        stressCharacterization      % stress characterization model
        storageReplacement          % [bool] time vector of storage replacements after EOL has been reached
        agingStress                 % [-] struct that saves detected cycles [cycleDepth, cRate, lastCycleEndStep]
        SOHCapacity                 % [pu] SOH of storage capacity
        SOHResistance               % [pu] SOH of storage resistance
        capacityChangeCalendric     % [Ws] capacity change due to calendric aging
        capacityChangeCyclic        % [Ws] capacity change due to cycle aging
        capacityChangeTotal         % [Ws] capacity change due to calendric and cycle aging
        resistanceChangeCalendric   % [Ohm] resistance change due to calendric aging
        resistanceChangeCyclic      % [Ohm] resistance change due to cycle aging
        resistanceChangeTotal       % [Ohm] resistance change due to calendric and cycle aging

        % auxilliary states and profiles
        powerStorageOp              % [W] power consumption required to operate storage device (sensors, controller, thermal management)
    end
    
    
    methods
        %% Constructor to create EESect of class storage
        function EES = storage( varargin )
            % constructor
            setProperties(EES, nargin, varargin{:});
        end
        
        % function to set SOC limits
        function [ EES ] = setSOCLim( EES, newSOCLim )
            if numel(newSOCLim)==2
                EES.SOCLimLow   = newSOCLim(1);
                EES.SOCLimHigh  = newSOCLim(2);
            end
        end
        
        % function to reset SOC limits to initial value
        function [ EES ] = resetSOCLim( EES )
            EES.SOCLimLow   = EES.inputTech.SOCLimLow;
            EES.SOCLimHigh  = EES.inputTech.SOCLimHigh;
        end
        
    end

    
    methods(Access = protected)
        % initialize object with parameters at first step
        function setupImpl(EES)
            % Set setPowerStorage method
            EES.setPowerStorage = EES.inputTech.setPowerStorageMethod;  % set fhandle as setPowerStorage method
            
            % Adapt battery rated power to power electronics rated power
            EES.inputTech.battRatedPower = EES.inputTech.powerElectronicsRatedPower / EES.inputTech.powerElectronicsEta(end); 
            
            %% Create eta as efficiency curves
            % Creates array with efficiency of power electronics (determines powerBatt)
            setupEtaPowerElectronics( EES );       
            % Creates array with efficiency of battery (determines dSOC)
            setupEtaBatt( EES );                                        

            % Initialize simulation time states
            EES.sampleTimeNow = 0;
            EES.timeNow = 0;
            EES.stepNow = 1;
            EES.lastStateVectorUpdateTimeNow = 0;
            
            % Initialize battery states           
            EES.powerStorageNow     = 0;
            EES.powerStorageOpNow   = 0;
            EES.powerBattNow        = 0;
            EES.SOCNow              = EES.inputTech.startSOC;           % start SOC
            EES.SOCLimHigh          = EES.inputTech.SOCLimHigh;         % upper SOC limit
            EES.SOCLimLow           = EES.inputTech.SOCLimLow;          % lower SOC limit
            EES.SOHCapacityNow      = EES.inputTech.sohCapacityStart;   % start SOH of storage capacity
            EES.SOHResistanceNow    = EES.inputTech.sohResistanceStart; % start SOH of storage resistance
            EES.powerStorageOp      = EES.inputTech.powerStorageOp;     % [W] power consumption required to operate storage device (sensors, controller, thermal management)     
            
            EES.totalRelCapacityChangeCalendricNow      = 0; % [Ws] capacity change due to calendric aging
            EES.totalRelCapacityChangeCyclicNow         = 0; % [Ws] capacity change due to cycle aging
            EES.totalRelCapacityChangeNow               = 0; % [Ws] capacity change due to calendric and cycle aging
            EES.totalRelResistanceChangeCalendricNow    = 0;  % [Ohm] resistance change due to calendric aging
            EES.totalRelResistanceChangeCyclicNow       = 0;  % [Ohm] resistance change due to cycle aging
            EES.totalRelResistanceChangeNow             = 0;  % [Ohm] resistance change due to calendric and cycle aging

            % Define number of steps for allocation of state properties
            simulationStepsVector   = zeros(1,ceil((EES.inputSim.simEnd - EES.inputSim.simStart)/EES.inputSim.sampleTime));          
            
            % Allocation of battery states
            EES.powerStorage        = simulationStepsVector; % [W] visible power of ES at inverter terminal (connection to grid) (charging positive, discharging negative) 
            EES.powerBatt           = simulationStepsVector; % [W] power seen by battery at terminal
            EES.SOC                 = simulationStepsVector; % [pu] state of charge
            EES.SOHCapacity         = simulationStepsVector; % [pu] SOH of storage capacity
            EES.SOHResistance       = simulationStepsVector; % [pu] SOH of storage resistance
            EES.temperature         = simulationStepsVector; % [K] Battery temperature
            EES.storageReplacement  = []; % [bool] time vector of storage replacements after EOL has been reached
           
            % Allocation of battery cell states 
            if(EES.inputSim.logBatteryEcOutput && strcmp(func2str(EES.inputTech.setPowerStorageMethod), 'setPowerStorageEquivalentCircuit'))                                                                    
                storageEcOutputStepsVector = simulationStepsVector;
            else
                storageEcOutputStepsVector = 0;                                                            % If logging is deactivated, no value of battery EC model is logged
            end

            EES.cellStates          = struct( ...                   % [-] Struct for logging of output values of battery EC model
                                        'powerLoss', storageEcOutputStepsVector, ...
                                        'voltage',   storageEcOutputStepsVector, ...
                                        'current',   storageEcOutputStepsVector, ...
                                        'eta',       storageEcOutputStepsVector, ...
                                        'ocv',       storageEcOutputStepsVector, ...
                                        'ri',        storageEcOutputStepsVector);

            % Allocation of battery aging trends
            if(EES.inputSim.logAgingResults)                                                                    
                agingStepsVector = simulationStepsVector;
            else
                agingStepsVector = 1; % logging is deactivated, only last value of aging is logged
            end
           
            EES.capacityChangeCalendric     = agingStepsVector; % [Ws] capacity change due to calendric aging
            EES.capacityChangeCyclic        = agingStepsVector; % [Ws] capacity change due to cycle aging
            EES.capacityChangeTotal         = agingStepsVector; % [Ws] capacity change due to calendric and cycle aging
            EES.resistanceChangeCalendric   = agingStepsVector; % [Ohm] resistance change due to calendric aging
            EES.resistanceChangeCyclic      = agingStepsVector; % [Ohm] resistance change due to cycle aging
            EES.resistanceChangeTotal       = agingStepsVector; % [Ohm] resistance change due to calendric and cycle aging
            
            % If aging should be neglected with agingModelType 'noaging',
            % noAging is forced to be selected for aging stress
            % characterization and aging calculation
            if(strcmp(EES.inputTech.agingModelType, 'noaging'))
                disp(['NoAging is selected as aging model type: ', ...
                        'No stress characterization is executed and no aging is calculated']);
                
                % Select NoAging as method to be called inside callAging method
                EES.inputTech.callMethodAgingModels = @callMethodAgingModels_noAging;

                % Select NoAging as method to be called when calculating total aging
                % inside the callMethodAgingModels method
                EES.inputTech.combAgingFct          = @combAgingType_noAging;
                
                % Logging of aging is deactivated if no aging is selected
                EES.inputSim.logAgingResults        = false;
            else
            
            % Create stress characterization object
            EES.stressCharacterization   = stressCharacterization( ...
                                    'CharacterizationMethod',   EES.inputTech.stressCharacterizationOptions.CharacterizationMethod, ...
                                    'sampleTime',               EES.inputTech.stressCharacterizationOptions.sampleTime, ...
                                    'SOCThreshold',             EES.inputTech.stressCharacterizationOptions.SOCThreshold,  ... % Threshold of SOC change between two time steps to detect cycle, should be bigger than self discharge
                                    'SOCSlopeThreshold',        EES.inputTech.stressCharacterizationOptions.SOCSlopeThreshold, ... % Threshold of SOC slope change between two 60s time steps to detect cycle: 0.01 = 0.6 C
                                    'DOCThreshold',             EES.inputTech.stressCharacterizationOptions.DOCThreshold, ... % Threshold for the minimum cycle depth that is applied in the half-cycle analysis
                                    'evalInitData2beCharacterizedString', EES.inputTech.stressCharacterizationOptions.evalInitData2beCharacterizedString, ...
                                    'evalData2beCharacterizedString', EES.inputTech.stressCharacterizationOptions.evalData2beCharacterizedString,...
                                    'evalLastSimLoopData2beCharacterizedString', EES.inputTech.stressCharacterizationOptions.evalLastSimLoopData2beCharacterizedString);
            end
            
%             % Initialize battery aging operation states
%             % agingStress states vector lengths are linked to step size of stress characterization
%             agingStateVectorLength   = zeros(1, EES.inputTech.stepSizeStressCharacterization);  
            
            EES.agingStress.lastCycle               = agingStepsVector;
            EES.agingStress.avgCRate                = agingStepsVector;
            EES.agingStress.avgSOC                  = agingStepsVector;
            EES.agingStress.meanSOC                 = agingStepsVector;
            EES.agingStress.minSOC                  = agingStepsVector;
            EES.agingStress.maxSOC                  = agingStepsVector;
            EES.agingStress.relCapacityThroughput   = agingStepsVector;

            % Initialize battery aging initial states
            EES.agingStress.avgSOC(1)                   = EES.inputTech.startSOC;
            EES.agingStress.lastCycle(1)                = 1; 
            EES.agingStress.cumAgingTime                = 0;
            EES.agingStress.cumRelCapacityThroughput    = 0; 
            
        end

        % run object for one step
        function [powerStorage, SOCNow, SOHCapacityNow] = stepImpl(EES, powerInput, simTime)
            % Update times and calculate storage action only if time step > 0
            EES.sampleTimeNow   = max(simTime - EES.timeNow, 0);
            EES.timeNow         = simTime;
            
            % update power states
            EES = EES.setPowerStorage(EES, powerInput);

            % check whether replacement takes place
            if( EES.SOHCapacityNow < EES.inputTech.agingModel.remainCapacityEOL )
                setReplacement( EES );
            end
            
            % Increment stepNow if simTime hits sampleTime. Arrays only
            % save data of sampleTime.
            if(EES.stepNow == 1 || ...
                EES.sampleTimeNow == EES.inputSim.sampleTime || ...
                (simTime - EES.lastStateVectorUpdateTimeNow) >= EES.inputSim.sampleTime)
                    EES.lastStateVectorUpdateTimeNow   = simTime;
                    EES.stepNow         = EES.stepNow + 1;
            end
            
            % Update output values
            powerStorage            = EES.powerStorageNow;
            SOCNow                  = EES.SOCNow;
            SOHCapacityNow          = EES.SOHCapacityNow;

        end
        
        %% Declaration of the methods in separate files  
        % construction methods
        [ EES ] = setupEtaBatt( EES )
        [ EES ] = setupEtaPowerElectronics( EES )
        % operation methods
        [ EES ] = setReplacement( EES )
        % aging methods
        [ EES ] = characterizeStress ( EES )
        [ EES ] = calcAging ( EES )
        
        %% Simulink methods
        % Define total number of inputs for system with optional inputs        
        function num = getNumInputsImpl(~)
            num = 2;
        end
        
        % Define total number of outputs for system with optional outputs
        function num = getNumOutputsImpl(~)    
            num = 3;
        end
        
        % Set properties in object obj to values in structure s
        function loadObjectImpl(EES,s,wasLocked)
            
            % Set private and protected properties
            loadFields = fieldnames(s);
            for k = 1:numel(loadFields)
                % Load only non discrete object states (DiscreteState are
                % labeld with 'Now' at the end of field name)
                if(~strcmp(loadFields{k}(end-2:end),'Now'))
                    EES.(loadFields{k}) = s.(loadFields{k});
                end
            end  

            % Set public properties and states
            loadObjectImpl@matlab.System(EES,s,wasLocked);
        end

        % Set properties in structure s to values in object obj
        function s = saveObjectImpl(EES)
            % Set public properties and states
            s = saveObjectImpl@matlab.System(EES);

            % Set private and protected properties
            saveFields = fieldnames(EES);
            for k = 1:numel(saveFields)
                % Save only non discrete object states (DiscreteState are
                % labeld with 'Now' at the end of field name)
                if(~strcmp(saveFields{k}(end-2:end),'Now'))
                    s.(saveFields{k}) = EES.(saveFields{k});
                end
            end  
        end

        % Return text as string or cell array of strings for the System
        % block icon
        function icon = getIconImpl(~)
            icon = mfilename('class'); % Use class name
        end

        % Return input port names for System block
        function [name, name2] = getInputNamesImpl(~)
            name    = 'powerInput';
            name2   = 'simTime';
        end

        % Return output port names for System block
        function [name,name2,name3] = getOutputNamesImpl(~)
            name    = 'powerStorage';
            name2   = 'SOC';
            name3   = 'SOH';
        end

        % Return size for each output port
        function [out,out2,out3] = getOutputSizeImpl(~)
            out = [1 1];
            out2 = [1 1];
            out3 = [1 1];
        end

        % Return data type for each output port
        function [out,out2,out3] = getOutputDataTypeImpl(~)
            out = 'double';
            out2 = 'double';
            out3 = 'double';
        end

        % Return true for each output port with complex data
        function [out,out2,out3] = isOutputComplexImpl(~)
            out = false;
            out2 = false;
            out3 = false;
        end

        % Return true for each output port with fixed size
        function [out,out2,out3] = isOutputFixedSizeImpl(~)
            out = true;
            out2 = true;
            out3 = true;
        end

        % Return size, data type, and complexity of discrete-state specified in name
        function [sz,dt,cp] = getDiscreteStateSpecificationImpl(~,~)
            sz = [1 1];
            dt = 'double';
            cp = false;
        end

    end % methods

    methods(Access = protected, Static)
        function simMode = getSimulateUsingImpl
            % Return only allowed simulation mode in System block dialog
            simMode = 'Interpreted execution';
        end
    end

end % classdef