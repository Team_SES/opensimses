%% setReplacement
% Resets the battery SOC, SOH and aging stress to the values given by the
% replacement data struct of the EES object
%
% Input/Output: EES object
%
% This functions is called when the battery reaches a certain end-of-life
% criteria, which is analyzed in every call of the step-function of the EES
% object.
%
% Resets battery's SOC, SOH and aging stress parameters of the EES object 
% to the values given by the replacement data. The replacement data is 
% defined when creating the EES object.
% The replacement action and time is logged with the parameter storageReplacement. 
%
% 2017-08-04   Maik Naumann
%
%%

function [ EES ] = setReplacement( EES )

% Update vector of storage replacement time 
EES.storageReplacement = [EES.storageReplacement EES.stepNow];

% Reset current SOC to start value
EES.SOCNow              = EES.inputTech.startSOC;
EES.SOC(EES.stepNow)    = EES.inputTech.startSOC;

% Reset storage capacity and SOH values to given values for replacement
EES.SOHCapacityNow      = EES.inputTech.replacementData.sohCapacity;       
EES.SOHResistanceNow    = EES.inputTech.replacementData.sohResistance;       

% If logging of aging stress is activated, current step is used for loggingIndex
if(EES.inputSim.logAgingResults)                                            
    idxLogging          = EES.stepNow;
else
    idxLogging          = 1;
end

%% Reset stress parameters for proper restart of stress detection

% Reset cumulative aging time to start from beginning of life of aging behavior
EES.agingStress.cumAgingTime            = EES.inputTech.replacementData.cumAgingTime;

% Reset cumulative relative charge througput to start from beginning of life of aging behavior
EES.agingStress.cumRelChargeThroughput  = EES.inputTech.replacementData.cumRelChargeThroughput; 

% Reset further stress parameters
EES.agingStress.relCapacityThroughput(idxLogging)   = 0;
EES.agingStress.lastCycle(idxLogging)               = 0;
EES.agingStress.minSOC(idxLogging)                  = 0;
EES.agingStress.maxSOC(idxLogging)                  = 0;
EES.agingStress.avgCRate(idxLogging)                = 0;
EES.agingStress.avgSOC(idxLogging)                  = 0;
EES.agingStress.meanSOC(idxLogging)                 = 0;

end

