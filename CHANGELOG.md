# Change log
All notable changes to this project will be documented in this file.

## [Unreleased]
- Aging model based on experiments on CLFP cells.
- Cuckoo-search optimized fuzzy-logic controller.
- Primary frequency control application.

## [1.1] - 2018-02-01
### Added
- GUI for straightforward simulation and result analysis.
- Battery replacement can now be determined after fixed time period instead of threshold SOH.

### Changed
- Additional method in residential class to modify economic input parameters.
- Script that adds required paths to matlab search path simplified and generalized. 
- Method for setting SOC-limit now catches input below 0 and above 1. 
- Call Aging methods simplified.
- New standard parameter values (economic and technical). 
- New scenarios for electricity prices and feed-in remuneration.
- New plots for aging and minor modifications.

### Fixed
- Recalculation of inverter power, in case battery cannot provide requested power.
- SOC limit variables united. Parameters of EC-model parameters (ecModelParams) now follow the object limits (ees.SOCLim*).
- Shifted calculation of inverse efficiency curve of inverter.
- Analysis script for economics checks for financial toolbox before calculating irr.
- Cycle aging values now correct.