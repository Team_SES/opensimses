%% setupEtaBatt
% Sub-Creator method to obtain efficiency vector for battery cell.
%
% creates array struct to assign powerBatt to according powerSOC. Done to
% consider losses within cell (rated capacity effect). Function is called
% during the parameterization at the first simulation step.
%
% Careful with very large power electronics. Extrapolation of battery
% efficiency may occur --> check if eta is within measured/expected values.
%
% 2017-08-03 Truong
%
%%

function [ EES ] = setupEtaBatt( EES )
% creates struct to assign powerBatt to dSOC
etaInput    = EES.inputTech.etaBatt;
inputSize   = size(etaInput);

% etaInput with discrete efficiency values according to given power ratios for charge and discharge direction
if inputSize(:) > 1
    cRateMaximum = EES.inputTech.battRatedPower/EES.inputTech.batteryNominalEnergy*3600;
    negEta      = etaInput(etaInput(:,1) <= 0, :);
    negEta      = interp1(negEta(:,1), negEta(:,2), linspace(-cRateMaximum,0,EES.inputTech.etaAccuracy+1),'phchip');
    negEta(negEta > 1) = 1;
    
    posEta      = etaInput(etaInput(:,1) >= 0, :);
    posEta      = interp1(posEta(:,1), posEta(:,2), linspace(0,cRateMaximum,EES.inputTech.etaAccuracy + 1),'phchip');
    posEta(posEta > 1) = 1;
    
    etaBatt     = [negEta, posEta(2:end)]; 
    if any(etaBatt == 1)
        warning('Efficiency of 1 for battery. Please check efficiency.')
    end
% etaInput with discrete efficiency values according to power ratio between 0 and 1 of charge direction
%% NT this elseif case never occurs because conditions are identical. What happened here?
elseif inputSize > 1
    x           = linspace(0, EES.inputTech.battRatedPower, EES.inputTech.etaAccuracy + 1); 
    etaInput_x  = linspace(0, EES.inputTech.battRatedPower, length(etaInput));
    negEta      = interp1(etaInput_x, 1./etaInput, x);
    negEta      = negEta(end:-1:2);
    posEta      = interp1(etaInput_x, etaInput, x);
    etaBatt     = [negEta, posEta];
    
% constant eta without power dependency    
else 
    negEta      = repmat(1./etaInput, 1, EES.inputTech.etaAccuracy);
    posEta      = repmat(etaInput, 1, EES.inputTech.etaAccuracy + 1);
    etaBatt     = [negEta, posEta];
end

% Write into object
EES.inputTech.etaBatt     = etaBatt(:);

end % end of function

