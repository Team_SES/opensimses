%% setReplacement
%   Resets the battery SOC, SOH and aging stress to the values given by the
%   replacement data struct of the EES object
%
%   Input/Output: EES object
%
%   This functions is called when the battery reaches a certain end-of-life
%   criteria, which is analyzed in every call of the step-function of the EES
%   object.
%
%   Resets battery's SOC, SOH and aging stress parameters of the EES object 
%   to the values given by the replacement data. The replacement data is 
%   defined when creating the EES object.
%   The replacement action and time is logged with the parameter storageReplacement. 
%
%   2017-08-04   Maik Naumann
%%
function [ ees ] = setReplacement( ees )

inputTech   = ees.inputTech;
agingStress = ees.agingStress;

% Update vector of storage replacement time 
ees.storageReplacement = [ees.storageReplacement ees.stepNow];

% Reset current SOC to start value
ees.SOCNow              = inputTech.startSOC;
ees.SOC(ees.stepNow)    = inputTech.startSOC;

% Reset storage capacity and SOH values to given values for replacement
ees.SOHCapacityNow      = inputTech.replacementData.sohCapacity;       
ees.SOHResistanceNow    = inputTech.replacementData.sohResistance;       

% If logging of aging stress is activated, current step is used for loggingIndex
if(ees.inputSim.logAgingResults)                                            
    idxLogging          = ees.stepNow;
else
    idxLogging          = 1;
end
disp(['Storage replacement at simulation time: ', num2str(ees.timeNow), ' seconds']);

%% Reset stress parameters for proper restart of stress detection
% Reset cumulative aging time to start from beginning of life of aging behavior
agingStress.cumAgingTime                = inputTech.replacementData.cumAgingTime;

% Reset cumulative relative charge througput to start from beginning of life of aging behavior
agingStress.cumRelCapacityThroughput    = inputTech.replacementData.cumRelChargeThroughput; 

% Reset further stress parameters
agingStress.lastCycle(idxLogging)       = 0;
agingStress.minSOC(idxLogging)          = 0;
agingStress.maxSOC(idxLogging)          = 0;
agingStress.avgCRate(idxLogging)        = 0;
agingStress.avgSOC(idxLogging)          = 0;

% Update object properties
ees.agingStress = agingStress;

end

