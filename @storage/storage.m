%% Class definition: storage
% Object type: Matlab object
% Class definition simulation of storage. The model includes operational
% limits (SOC, power), losses and aging.
%
% Call via name-value pair
% ees = storage('inputSim', inputSim, 'inputTech', inputTech, 'economicData', economicData)
%
% Output:
% - ees: object for simulation
%
% Input:
% - inputSim: struct with simulation parameters in respective struct-fields
% - inputTech: struct with technical parameters of the storage system 
%   and its aux. components.
% - economicData: struct with economic input data of storage system
%
% Class definition for simulation of storage system.
% 
% Operational limits (SOC and power) are checked within setPower method and
% do not require consideration in the control algorithm.
% Object states are updated after reference power is given. Submodels for
% aging, aux. components, battery are used for computation of state
% changes.
%
% 2017-08-04 Naumann/Truong

classdef storage < handle

    %%% Properties and arrays to save history of states etc.
    properties(GetAccess = public, SetAccess = private, Hidden = false)
        %% Input data
        inputSim                    % Input parameter struct for simulation configuration of storage object
        inputTech                   % Input parameter struct for technical configuration of storage object
    
        %% momentary states
        powerStorageNow             % [W] Current visible power of ES at inverter terminal (connection to grid) (charging positive, discharging negative)         
        powerBattNow                % [W] Current power seen by battery at terminal / DC-side of inverter
        powerStorageOpNow           % [W] Current power consumption required to operate storage device (sensors, controller, thermal management)
        SOCNow                      % [pu] current SOC
        SOHCapacityNow              % [pu] Current SOH of storage capacity
        SOHResistanceNow            % [pu] Current SOH of storage resistance
        
        totalRelCapacityChangeCalendricNow  % [Ws] capacity change due to calendric aging
        totalRelCapacityChangeCyclicNow     % [Ws] capacity change due to cycle aging
        totalRelCapacityChangeNow           % [Ws] capacity change due to calendric and cycle aging
        totalRelResistanceChangeCalendricNow    % [Ohm] resistance change due to calendric aging
        totalRelResistanceChangeCyclicNow       % [Ohm] resistance change due to cycle aging
        totalRelResistanceChangeNow             % [Ohm] resistance change due to calendric and cycle aging

        timeNow                     % [s] Time since start of simulation
        stepNow                     % [~] Steps since start of simulation

        sampleTimeNow               % [s] sample time of current step
        lastStateVectorUpdateTimeNow% [s] time of last state vector update 
    
        setPowerStorage = @(x) [];  % Function handle to determine how SOC etc. is handled (EC or power flow)
        
        %% arrays to save history of states etc.
        % Battery states
        powerStorage                % [W] visible power of ES at inverter terminal (connection to grid) (charging positive, discharging negative) 
        powerBatt                   % [W] power seen by battery at terminal
        SOC                         % [pu] state of charge
        SOCLimHigh                  % [pu] upper SOC limit --> tunable for some EMS
        SOCLimLow                   % [pu] lower SOC limit
        cellStates                  % [-] Struct for logging of output values of battery EC model
        temperature                 % [K] Battery temperature

        % Aging trends
        stressCharacterization      % stress characterization model
        storageReplacement          % [bool] time vector of storage replacements after EOL has been reached
        agingStress                 % [-] struct that saves detected cycles [cycleDepth, cRate, lastCycleEndStep]
        SOHCapacity                 % [pu] SOH of storage capacity
        SOHResistance               % [pu] SOH of storage resistance
        capacityChangeCalendric     % [Ws] capacity change due to calendric aging
        capacityChangeCyclic        % [Ws] capacity change due to cycle aging
        capacityChangeTotal         % [Ws] capacity change due to calendric and cycle aging
        resistanceChangeCalendric   % [Ohm] resistance change due to calendric aging
        resistanceChangeCyclic      % [Ohm] resistance change due to cycle aging
        resistanceChangeTotal       % [Ohm] resistance change due to calendric and cycle aging

        % auxilliary states and profiles
        powerStorageOp              % [W] power consumption required to operate storage device (sensors, controller, thermal management)
    end
    
    methods
        
        %% Constructor to create EES object of class storage
        function EES = storage( varargin )
            % constructor
            setProperties(EES, varargin{:});  
            setupImpl(EES)
        end
        
         % run object for one step
        function [powerStorage, SOCNow, SOHCapacityNow] = step(EES, powerInput, simTime)
            % Update times and calculate storage action only if time step > 0
            EES.sampleTimeNow   = max(simTime - EES.timeNow, 0);
            EES.timeNow         = simTime;
            
            % Update power states if storage capacity is still available
            if ( EES.SOHCapacityNow > 0.001)
                EES = EES.setPowerStorage(EES, powerInput);
            else
                % Set default values
                EES.SOCNow                  = 0;                
                EES.powerStorageNow         = 0;        
                EES.powerBattNow            = 0;  
            end
            
            % Check whether storage replacement takes place:
            if ( EES.SOHCapacityNow < EES.inputTech.agingModel.remainCapacityEOL )
                    setReplacement( EES );
            % With configured replacement interval
            elseif( simTime > EES.inputTech.scheduledNextReplacement && EES.inputTech.scheduledNextReplacement ~= 0)
                setReplacement( EES );
                % Determine next replacement time with configured replacement interval
                EES.inputTech.scheduledNextReplacement = simTime + EES.inputTech.replacementInterval;
            end
            
            % Increment stepNow if simTime hits sampleTime. Arrays only
            % save data of sampleTime.
            if(EES.stepNow == 1 || ...
                EES.sampleTimeNow == EES.inputSim.sampleTime || ...
                (simTime - EES.lastStateVectorUpdateTimeNow) >= EES.inputSim.sampleTime)
                    EES.lastStateVectorUpdateTimeNow   = simTime;
                    EES.stepNow         = EES.stepNow + 1;
            end
            
            % Update output values
            powerStorage            = EES.powerStorageNow;
            SOCNow                  = EES.SOCNow;
            SOHCapacityNow          = EES.SOHCapacityNow;
        end
              
        % function to set SOC limits
        function [ EES ] = setSOCLim( EES, newSOCLim )
            if numel(newSOCLim)==2
                if(newSOCLim(2) > 1)
                    warning('SOCLimHigh cannot exceed 1, set to 1.')
                    EES.SOCLimHigh = 1;
                else
                    EES.SOCLimHigh = newSOCLim(2);         % upper SOC limit
                end
            
                if(newSOCLim(1) < 0)
                    warning('SOCLimLow cannot go below 0, set to 0.')
                    EES.SOCLimLow = 0;
                else
                    EES.SOCLimLow = newSOCLim(1);          % lower SOC limit
                end
            end
        end
        
        % function to reset SOC limits to initial value
        function [ EES ] = resetSOCLim( EES )
            EES.SOCLimLow   = EES.inputTech.SOCLimLow;
            EES.SOCLimHigh  = EES.inputTech.SOCLimHigh;
        end
        
    end

    methods(Access = protected)
        % Set input properties
        function setProperties(EES, varargin)
            
            %% parse input and check if input vars are complete
            p = inputParser;
            defVal = NaN;

            addParameter(p, 'inputSim',     defVal);
            addParameter(p, 'inputTech',    defVal);

            parse(p,varargin{:});

            EES.inputSim    = p.Results.inputSim;
            EES.inputTech   = p.Results.inputTech;
        end
        
        % initialize object with parameters at first step
        function setupImpl(EES)
            % Set setPowerStorage method
            EES.setPowerStorage = EES.inputTech.setPowerStorageMethod;  % set fhandle as setPowerStorage method
            
            % Adapt battery rated power to power electronics rated power
            EES.inputTech.battRatedPower = EES.inputTech.powerElectronicsRatedPower / EES.inputTech.powerElectronicsEta(end); 
            
            %% Create eta as efficiency curves
            % Creates array with efficiency of power electronics (determines powerBatt)
            setupEtaPowerElectronics( EES );       
            % Creates array with efficiency of battery (determines dSOC)
            setupEtaBatt( EES );                                        

            % Initialize simulation time states
            EES.sampleTimeNow = 0;
            EES.timeNow = 0;
            EES.stepNow = 1;
            EES.lastStateVectorUpdateTimeNow = 0;
            
            % Initialize battery states           
            EES.powerStorageNow     = 0;
            EES.powerStorageOpNow   = 0;
            EES.powerBattNow        = 0;
            EES.SOCNow              = EES.inputTech.startSOC;           % start SOC
            
            if(EES.inputTech.SOCLimHigh > 1)
                error('SOCLimHigh should be below 1')
            else
                EES.SOCLimHigh          = EES.inputTech.SOCLimHigh;         % upper SOC limit
            end
            
            if(EES.inputTech.SOCLimLow < 0)
                error('SOCLimLow should be above 0')
            else
                EES.SOCLimLow           = EES.inputTech.SOCLimLow;          % lower SOC limit
            end

            EES.SOHCapacityNow      = EES.inputTech.sohCapacityStart;   % start SOH of storage capacity
            EES.SOHResistanceNow    = EES.inputTech.sohResistanceStart; % start SOH of storage resistance
            EES.powerStorageOp      = EES.inputTech.powerStorageOp;     % [W] power consumption required to operate storage device (sensors, controller, thermal management)     
            
            EES.totalRelCapacityChangeCalendricNow      = 0; % [Ws] capacity change due to calendric aging
            EES.totalRelCapacityChangeCyclicNow         = 0; % [Ws] capacity change due to cycle aging
            EES.totalRelCapacityChangeNow               = 0; % [Ws] capacity change due to calendric and cycle aging
            EES.totalRelResistanceChangeCalendricNow    = 0;  % [Ohm] resistance change due to calendric aging
            EES.totalRelResistanceChangeCyclicNow       = 0;  % [Ohm] resistance change due to cycle aging
            EES.totalRelResistanceChangeNow             = 0;  % [Ohm] resistance change due to calendric and cycle aging

            % Define number of steps for allocation of state properties
            simulationStepsVector   = zeros(1,ceil((EES.inputSim.simEnd - EES.inputSim.simStart)/EES.inputSim.sampleTime));          
            
            % Allocation of battery states
            EES.powerStorage        = simulationStepsVector; % [W] visible power of ES at inverter terminal (connection to grid) (charging positive, discharging negative) 
            EES.powerBatt           = simulationStepsVector; % [W] power seen by battery at terminal
            EES.SOC                 = simulationStepsVector; % [pu] state of charge
            EES.SOHCapacity         = simulationStepsVector; % [pu] SOH of storage capacity
            EES.SOHResistance       = simulationStepsVector; % [pu] SOH of storage resistance
            EES.temperature         = simulationStepsVector; % [K] Battery temperature
            EES.storageReplacement  = []; % [bool] time vector of storage replacements after EOL has been reached
                  
            % Allocation of battery cell states 
            if(EES.inputSim.logBatteryEcOutput && strcmp(func2str(EES.inputTech.setPowerStorageMethod), 'setPowerStorageEquivalentCircuit'))                                                                    
                storageEcOutputStepsVector = simulationStepsVector;
            else
                storageEcOutputStepsVector = 0;                                                            % If logging is deactivated, no value of battery EC model is logged
            end

            EES.cellStates          = struct( ...                   % [-] Struct for logging of output values of battery EC model
                                        'powerLoss', storageEcOutputStepsVector, ...
                                        'voltage',   storageEcOutputStepsVector, ...
                                        'current',   storageEcOutputStepsVector, ...
                                        'eta',       storageEcOutputStepsVector, ...
                                        'ocv',       storageEcOutputStepsVector, ...
                                        'ri',        storageEcOutputStepsVector);

            % Allocation of battery aging trends
            if(EES.inputSim.logAgingResults)                                                                    
                agingStepsVector = simulationStepsVector;
            else
                agingStepsVector = 0; % logging is deactivated, only last value of aging is logged
            end

            % If aging should be neglected with agingModelType 'noaging',
            % noAging is forced to be selected for aging stress
            % characterization and aging calculation
            if(strcmp(EES.inputTech.agingModelType, 'noaging'))
                disp(['NoAging is selected as aging model type: ', ...
                        'No stress characterization is executed and no aging is calculated']);
                
                % Select NoAging as method to be called inside callAging method
                EES.inputTech.callMethodAgingModels = @callMethodAgingModels_noAging;

                % Select NoAging as method to be called when calculating total aging
                % inside the callMethodAgingModels method
                EES.inputTech.combAgingFct          = @combAgingType_noAging;
                
                % Logging of aging is deactivated if no aging is selected
                EES.inputSim.logAgingResults        = false;
            else
            
            % Create stress characterization object
            EES.stressCharacterization   = stressCharacterization( ...
                                    'CharacterizationMethod',   EES.inputTech.stressCharacterizationOptions.CharacterizationMethod, ...
                                    'sampleTime',               EES.inputTech.stressCharacterizationOptions.sampleTime, ...
                                    'SOCThreshold',             EES.inputTech.stressCharacterizationOptions.SOCThreshold,  ... % Threshold of SOC change between two time steps to detect cycle, should be bigger than self discharge
                                    'SOCSlopeThreshold',        EES.inputTech.stressCharacterizationOptions.SOCSlopeThreshold, ... % Threshold of SOC slope change between two 60s time steps to detect cycle: 0.01 = 0.6 C
                                    'DOCThreshold',             EES.inputTech.stressCharacterizationOptions.DOCThreshold, ... % Threshold for the minimum cycle depth that is applied in the half-cycle analysis
                                    'evalInitData2beCharacterizedString', EES.inputTech.stressCharacterizationOptions.evalInitData2beCharacterizedString, ...
                                    'evalData2beCharacterizedString', EES.inputTech.stressCharacterizationOptions.evalData2beCharacterizedString,...
                                    'evalLastSimLoopData2beCharacterizedString', EES.inputTech.stressCharacterizationOptions.evalLastSimLoopData2beCharacterizedString);
            end
            
            % Initialize battery aging operation states
            EES.capacityChangeCalendric             = agingStepsVector;
            EES.capacityChangeCyclic                = agingStepsVector;
            EES.capacityChangeTotal                 = agingStepsVector;
            EES.resistanceChangeCalendric           = agingStepsVector;
            EES.resistanceChangeCyclic              = agingStepsVector;
            EES.resistanceChangeTotal               = agingStepsVector;
            
            EES.agingStress.lastCycle               = agingStepsVector;
            EES.agingStress.avgCRate                = agingStepsVector;
            EES.agingStress.avgSOC                  = agingStepsVector;
            EES.agingStress.meanSOC                 = agingStepsVector;
            EES.agingStress.minSOC                  = agingStepsVector;
            EES.agingStress.maxSOC                  = agingStepsVector;

            % Initialize battery aging initial states
            EES.agingStress.avgSOC(1)                   = EES.inputTech.startSOC;
            EES.agingStress.lastCycle(1)                = 1; 
            EES.agingStress.cumAgingTime                = 0;
            EES.agingStress.cumRelCapacityThroughput    = 0;    
        end

        %% Declaration of the methods in separate files  
        % construction methods
        [ EES ] = setupEtaBatt( EES )
        [ EES ] = setupEtaPowerElectronics( EES )
        % operation methods
        [ EES ] = setReplacement( EES )
        % aging methods
        [ EES ] = characterizeStress ( EES )
        [ EES ] = calcAging ( EES )
        
    end  
end % classdef