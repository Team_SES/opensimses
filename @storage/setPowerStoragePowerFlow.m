%% setPowerStorage 
% Method sets the method of the storage system within allowed limits.
% Rated power of inverter is considered. Invoked functions also check the
% SOC-limits and limit the powerStorage for the system to remain within 
% allowed operation limits.
% Efficiency losses of the power electronics and the battery is included.
% Proposed method to control the energy storage system.
%   
% 2017-08-03 Truong
%   
%%


function [ EES ] = setPowerStoragePowerFlow( EES, powerStorageNow )
% multiple used properties written in variables (execution speed)
stepNow                     = EES.stepNow;
sampleTime                  = EES.sampleTimeNow;
etaAccuracy                 = EES.inputTech.etaAccuracy;
powerElectronicsRatedPower  = EES.inputTech.powerElectronicsRatedPower;
battRatedPower              = EES.inputTech.battRatedPower;
SOCnow                      = EES.SOCNow;
SOCLimLow                   = EES.SOCLimLow;
SOCLimHigh                  = EES.SOCLimHigh;
sohCapacityNow              = EES.SOHCapacityNow;
sohResistanceNow            = EES.SOHResistanceNow;
battSelfDischRate           = EES.inputTech.selfDischargeRate;

% add system standbyLoad to inverter power
stbyLoad        = 0; % dummy of 0 watts --> model required
powerInverter   = powerStorageNow - stbyLoad;

% limit reference power to rated power of power electronics
powerInverter   = max(powerInverter, -powerElectronicsRatedPower);
powerInverter   = min(powerInverter,  powerElectronicsRatedPower); 


%% calculate power at battery terminal
% calculates the power at the battery terminals after PowerElectronics losses
etaIdx          = ceil( etaAccuracy * ( powerInverter / powerElectronicsRatedPower + 1 ) + 1 );
etaPENow        = EES.inputTech.etaPowerElectronics( etaIdx );
powerBattT      = etaPENow * powerInverter; 

% check for limits of battery or power electronics
powerBattNow    = min( powerBattT,    battRatedPower );
powerBattNow    = max( powerBattNow, -battRatedPower );
powerBattNow    = min( powerBattNow,  powerElectronicsRatedPower );
powerBattNow    = max( powerBattNow, -powerElectronicsRatedPower );
% if power is adjusted to limit, recalculate pAc of inverter
if (diff([abs(powerBattT),abs(powerBattNow)])/powerElectronicsRatedPower > 0.01) || powerBattNow > powerStorageNow
    etaIdx          = ceil( etaAccuracy * ( powerBattNow / battRatedPower + 1 ) + 1 );
    etaPENow        = EES.inputTech.etaPEInverse( etaIdx );
    powerInverter   = etaPENow * powerBattNow;
end

%% calculate SOC
% calculates the resulting SOC depending on the power at battery terminals
etaIdx      = ceil( etaAccuracy * ( powerBattNow/battRatedPower +1 ) + 1 ); % obtain normalized power and respective array idx for efficiency curve
etaBattNow  = EES.inputTech.etaBatt( etaIdx );                              % get efficiency value for power value
powerSOC    = etaBattNow * powerBattNow;                                    % calculate power in battery cell
% dSOC        = powerSOC / ( EES.SOHCapacityNow * EES.inputTech.batteryNominalEnergy ) * sampleTime + dSocSelfDischarge;   % calculate delta SOC
dSOC        = powerSOC / ( EES.SOHCapacityNow * EES.inputTech.batteryNominalEnergy ) * sampleTime;   % calculate delta SOC
newSOC      = dSOC + SOCnow;                                                % calculate new possible SOC, if no thresholds are violated

% no more charging if SOC == 1, no more discharging if SOC == 0
if ((dSOC <= 0) && (SOCnow == SOCLimLow))||((dSOC >= 0) && (SOCnow == SOCLimHigh))
    newSOC          = SOCnow;   % SOC unchanged
    powerInverter   = 0;        % no powerStorage because SOC is at limit already
    powerBattNow    = 0;        % calculate powerBatt accordingly

% If chosen power would lead to exceeding SOC, powers need to be reduced.
% Assumption: Full provision of requested power until SOC-limit is reached.
% Power output is then reduced to 0, thus avg. power of time-step is used.
elseif ((newSOC < SOCLimLow) || (newSOC > SOCLimHigh))
    % Self discharge should decrease SOC, if SOCLimLow > 0
    if(SOCLimLow > 0 && powerBattNow == 0)
        newSOC = dSOC + SOCnow;
    else
        newSOC          = max(newSOC,SOCLimLow);        % SOC within lower boundary
        newSOC          = min(newSOC,SOCLimHigh);       % SOC within upper boundary
        limFactor       = (newSOC - SOCnow) / dSOC;     % ratio to reduce powers at current timestep
        % if limfactor = inf or nan, set to 0
        if (isnan(limFactor) || isinf(limFactor))
            limFactor = 0;
        end
        powerInverter   = limFactor * powerInverter;  % reduce powerStorage to achieve SOC limit
        powerBattNow    = limFactor * powerBattNow;     % calculate powerBatt accordingly
    end
end 

%% calculate self discharge
% Assumption: 
%   - Self discharge occurs always: Equally in idle periods
if powerBattNow == 0
    dSocSelfDischarge = - battSelfDischRate * sampleTime * 1/sohCapacityNow;
    newSOC  = max(SOCnow + dSocSelfDischarge, 0);
end

% calculate the output of the storage system (inverter - stby)
powerStorageNow = powerInverter + stbyLoad;

% calculate temperature of battery for aging ambient temperature is assumed
% for now. 
% Placeholder for thermal model
temperatureBatt = EES.inputTech.temperatureAmbient;

% update of object properties
EES.SOCNow                  = newSOC;
EES.SOC(stepNow)            = newSOC;                   
EES.powerStorageNow         = powerStorageNow;
EES.powerStorage(stepNow)   = powerStorageNow;          
EES.powerBattNow            = powerBattNow;  
EES.powerBatt(stepNow)      = powerBattNow;
EES.temperature(stepNow)    = temperatureBatt;

% compute battery stress and resulting aging
characterizeStress( EES );
calcAging( EES ); 

end % end of function

