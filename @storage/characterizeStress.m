%% characterizeStress
%
%   Calls the stress characterization method defined by the stress 
%   characterization configuration
%
%   Input/Output: EES object
%
%   This functions calls the stress characterization method, which is defined
%   within the stressCharacterization object. Dependent on the current
%   simulation step, the stress characerization is called with individual
%   configurations.
%   The characterized stress values are then stored inside the
%   stressCharacterization object and used afterwards in the calcAging 
%   function.
%
%   2017-08-04   Maik Naumann
%%
function [ EES ] = characterizeStress( EES )

% If aging should be neglected with agingModelType 'NoAging', no stress
% characterization is executed and consequently no aging is calculated
if(strcmp(EES.inputTech.agingModelType, 'noaging'))
    % No stress characterization is executed in this case

% If other aging models are selected, stress characterization is executed
% as defined with the stressCharacterization object SC inside EES
else
    % Check if final simulation step
    if EES.timeNow == EES.inputSim.simEnd
        finalSimLoop = true;
    else 
        finalSimLoop = false;
    end

    % Initalize once in first simulation step
    if(EES.stepNow == 1)
        % Characterization setup
        EES.stressCharacterization.Data2beCharacterized   = eval(EES.stressCharacterization.evalInitData2beCharacterizedString); % [];

        % Call defined characterization method
        EES.stressCharacterization.CharacterizationMethod(EES.stressCharacterization);

    % Call stress characterization at the end of simulation with special data selection
    elseif(finalSimLoop)
        % Characterization setup
        EES.stressCharacterization.finalSimLoop           = true;
        EES.stressCharacterization.Data2beCharacterized   = eval(EES.stressCharacterization.evalLastSimLoopData2beCharacterizedString);

        % Call defined characterization method
        EES.stressCharacterization.CharacterizationMethod(EES.stressCharacterization);

    % Call stress characterization with data selection by defined step size 
    elseif(mod(EES.stepNow, EES.inputTech.stepSizeStressCharacterization) == 0)   
        % Characterization setup
        EES.stressCharacterization.Data2beCharacterized   = eval(EES.stressCharacterization.evalData2beCharacterizedString); %Data2beCharacterized; [time, value]

        % Call defined characterization method
        EES.stressCharacterization.CharacterizationMethod(EES.stressCharacterization);
    end

end

