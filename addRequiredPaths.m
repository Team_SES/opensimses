%% addRequiredPaths
% Script to add relevant folder into Matlab's search path in order to
% correctly access scripts and functions.
% Missing folders are added automatically.
%
%
% 2017-08-07 Truong
%%
% Create dir if it does not exist yet
folders = {'01_ProfileData/', '07_Results/', '11_Resources/'};

for k = 1:numel(folders)
    checkFolder(folders{k});
end

% Add necessary paths
addpath(genpath(fileparts(which('addRequiredPaths.m'))));
rmpath([pwd '/.git']);


function [] = checkFolder( name )
if ~exist(name,'dir')
    mkdir(name)
end
end