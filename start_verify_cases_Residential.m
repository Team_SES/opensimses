%% Example simulate residential
% Example script to start simulation of residential storage. Calling of
% subclass _residential_ for specific application of increasing a
% prosumer's self-consumption.
% 
% Direct call in MATLAB without use of Simulink environment. Check other
% example simulation script for call of system object in Simulink
% environment.
%%
clear

%% Preparing MATLAB
% Setting up Matlab search path and global helping variables.

% Global helping variables for conversion
global gvarYEARS2SECONDS gvarDAYS2SECONDS gvarKWH2WS
gvarYEARS2SECONDS   = 3600 * 24 * 365;  % helping variable to convert between years and seconds
gvarDAYS2SECONDS    = 3600 * 24;        % helping variable to convert between days and seconds
gvarKWH2WS          = 3600e3;           % helping variable to convert between kWh and Ws

% Run script to add required subfolders to MATLAB's search path. 
run('addRequiredPaths.m')   

%% Input profiles
% Load chosen profile data into workspace. Used function checks for errors.
% If given load profiles do not exist in folder, please download from
% SimSES Bitbucket download area before or during the function call.
localFilesFolder    = '01_ProfileData';
generationProfile   = returnInputProfile(...
                        'profileFileName',      'PV_EEN_Power_Munich_2014.mat', ...
                        'profileServerPath',    'https://bitbucket.org/Team_SES/opensimses/downloads/', ...
                        'localFilesFolder',     localFilesFolder, ...
                        'verifyHashCode',       false);
                    
loadProfile         = returnInputProfile(...
                        'profileFileName',      'EEN_100_RES_Load_Profiles_1min_1a.mat', ...
                        'profileServerPath',    'https://bitbucket.org/Team_SES/opensimses/downloads/', ...
                        'localFilesFolder',     localFilesFolder, ...
                        'verifyHashCode',       false, ...
                        'profileNumber',        1);
                    
%% Configuration of simulation
% Setting of necessary input parameters. Structs hold parameters with
% according fieldnames. Struct fieldnames cannot be changed.
%
% * inputSim
% * inputTech
% * inputEconomics

%% Simulation parameters
% Simulation parameters _inputSim_. Sample time, simulated time and logging
% flags are set here.

inputSim.simStart           = 0 * gvarDAYS2SECONDS;         % [s] starting time of simulation 

inputSim.simEnd             = 5/365 * gvarYEARS2SECONDS;        % [s] end time of simulation 
inputSim.sampleTime         = 15 * 60;                       % [s] sample time of simulation 

inputSim.loadProfileLength  = 1 * gvarYEARS2SECONDS;        % [s] length of input load profile 
inputSim.genProfileLength   = 1 * gvarYEARS2SECONDS;        % [s] length of input generation profile 

inputSim.plotFrom           = inputSim.simStart;            % [s] starting time of plot 
inputSim.plotTo             = inputSim.simEnd;              % [s] last time to be included in plot 
inputSim.plotTimeUnit       = 'days';                       % [-] depicts time unit for plotting (ticks of x-axis)

inputSim.saveResults        = true;                        % TRUE: the variables will be saved in a .mat-file
inputSim.logBatteryEcOutput = true;                        % TRUE: all output values of equivalent circuit (EC) model are logged
inputSim.logAgingResults    = true;                        % TRUE: all output values of _detectStress_ are logged


%% Technical parameters
% Technical parameters _inputTech_. Check fieldnames of variable to set
% desired desired values.

inputTech = techParamStdPVHomeStorage();                    % generates standard struct for required parameters

%%
% *Modify standard technical parameters:*

%%
% Household configuration is changed from standard case.
inputTech.PVPeakPower           = 10e3;                     % [W] installed capacity of photovoltaic unit
inputTech.batteryNominalEnergy  = 4 * gvarKWH2WS;           % [Ws] single value or array
inputTech.PVCurtailment         = 0.5;                      % [p.u.] max. feed-in power (kW) per kW installed PV capacity

%%
% Choose control strategy / operation strategy (OS). In this case
% conventional greedy strategy for maximum self-consumption rate is
% utilized for simulation.

% Array for all storageOS
storageOS                       = {@OSPVHomeGreedy, @OSDynamicFeedInLimit, @OSPVHomeMaxSCNoCurtail, @OSFeedInDamping};

for idxOS = 1:numel(storageOS)
inputTech.storageOS             = storageOS{idxOS};          % available OS (found in classfolder ('/06_Subfunctions/operationStrategies/')

%%
% Determine aging model and battery type

% Array for all aging model types
agingModelType = {'clfp_goebel', 'lib_rosenkranz', 'nmc_tesla_dailycyclepowerwall', 'noaging'};
for idxAging = 1:numel(agingModelType)
inputTech.agingModelType        = agingModelType{idxAging};                        % aging model 

% Array for all battery types
batteryType = {'clfp_goebel', 'nmc_tesla_dailycyclepowerwall', 'clfp_sony_us26650_experiment', 'clfp_sony_us26650_experiment_ocv_r'};

for idxBatteryType = 1:numel(batteryType)
inputTech.batteryType           = batteryType{idxBatteryType}; % battery type

%% Creating advanced parameter
% Run script that uses pre-determined input parameters to generate more
% complicated input data (price development, efficiency curves, ...)
run('createTechParamPVHomeStorage.m')

%% Generate object and run simulation
% Call class constructor method to generate object. Constructor is executed
% with parameter value pair method of MATLAB.
ees = residential(  'inputSim',         inputSim,       ...
                     'inputTech',       inputTech,      ...
                     'inputProfiles',   inputProfiles,  ...
                     'inputForecast',   inputForecast   );
                
%%
% Run simulation with generated object.
disp('Start Matlab Simulation')         % Display start of simulation at command window 
tic   
runStorage( ees );                      % call run storage method for simulation run
toc
disp('Simulation complete'); 

%% Evaluation and analysis of results
% Call evaluation functions for analysis of simulation.
disp('Evaluating:')
ees = evalTechnicalResidential( ees );  % calculate technical assessment values


%% Economic calculations
% After technical simulation, economic assumptions can be modified and the
% system's economic value assessed for the specific application.


%% Economic parameters
% Generate economic parameters _economicInput_.

% generates standard struct for required parameters
inputEconomics = econParamStdPVHomeStorage();

storageCost = {'min', 'pbs_optimizationpaper', 'lfp_optimizationpaper', 'nmc_optimizationpaper', 'tesla_ntpaper'};
scenarioElectricityPrice = {'constant','linear'};
scenarioRemunerationRate = {'constant'};
for idxStorageCost = 1:numel(storageCost)
    
for idxElectricityPrice = 1:numel(scenarioElectricityPrice)
    
for idxRemunerationRate = 1:numel(scenarioRemunerationRate)
    
inputEconomics.general.scenarioStorageCosts = storageCost{idxStorageCost};  % expected scenarios or battery choice: 'Max' ,'Min',... 
inputEconomics.pvHome.scenarioElectricityPrices   = scenarioElectricityPrice{idxElectricityPrice};           % scenarios: 'Constant', 'Linear', 'Break-Even'  
inputEconomics.pvHome.scenarioFeedInRemuneration  = scenarioRemunerationRate{idxRemunerationRate};         % scenarios: 'constant'  

%%
% *Modify standard economic parameters:*
% Depreciation period set to simulated time.
inputEconomics.general.depreciationPeriod = ceil(ees.inputSim.simEnd / gvarYEARS2SECONDS);

%%
% Create cost for storage system.
inputEconomics = createStorageCosts( inputEconomics ); 

%%
% create application specific economic parameters 
inputEconomics = createElectricityPrices( inputEconomics );
inputEconomics = createFeedInRemuneration( inputEconomics );

%% Calculate economic results
% Calculate economic key figures (LCOE, NPV, ...) with method of
% residential class.
ees = evalEconomics( ees, inputEconomics );


%%
% Saving workspace variables.

% Create dir if it does not exist yet
if ~exist('07_Results','dir')
    mkdir('07_Results')
end

% Save ees object.
if inputSim.saveResults == 1
    save(['07_Results\results_',datestr(now,30),'.mat'], 'ees');
    disp('Results saved.') 
end

end % end remuneration rate

end % end electricity price

end % end storage cost

end % end battery type

end % end aging model type

end % end OS


%% Plotting and saving of data
% Plotting of profiles (power flows, SOC, SOH) for quick analysis.
disp('Plotting.')
plotStorageData( ees, 'figureNo',       1,                                      ...
                        'timeFrame',    [inputSim.plotFrom inputSim.plotTo],    ...
                        'timeUnit',     inputSim.plotTimeUnit                   ); 
                    
plotResidentialProfile( ees, 'figureNo', 2,                                      ...
                        'timeFrame',    [inputSim.plotFrom inputSim.plotTo],    ...
                        'timeUnit',     inputSim.plotTimeUnit                   );

