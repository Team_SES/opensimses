%% plotStorageData
% plotStorageData plots storage power, SOC, SOH and Heat Maps of the
% storage power. Four plots in one figure. This function calls the
% different plot functions for SOC, SOH and Power.
%
% INPUTS 
%   EES: Storage object (obligatory)
%   figureNo, timeFrame, timeUnit (see below in USAGE)
%
% OUTPUTS: none
%
% USAGE: plotStorageDataSO(
%           EES{<number>}, 
%           'figureNo', <number of figures already plotted + 1>, 
%           'timeFrame', <time frame that is plotted in seconds>, 
%           'timeUnit', <desired time unit of x axis>)
%
% 15.01.2018 Markus F�rstl

function [ ] = plotStorageData( EES, varargin )
%% Get default figure settings
run('figureSettingsDefault.m')

%% Prepare input data
global gvarYEARS2SECONDS gvarDAYS2SECONDS

p = inputParser; % Input Parser to handle parameter inputs

% default values in case parameter is not set
expectedTimeUnit    = {'years', 'days', 'hours', 'minutes', 'seconds', ...
                        'year', 'day', 'hour', 'minute', 'second'}; % limiting possible time units

defVal = NaN; 

addParameter(p, 'timeFrame', defVal); 
addParameter(p, 'timeUnit', defVal, @(x) any(validatestring(x, expectedTimeUnit))); 
addParameter(p, 'figureNo', defVal); 

parse(p, varargin{:}); 

timeFrame   = p.Results.timeFrame;
timeUnit    = p.Results.timeUnit;
figureNo    = p.Results.figureNo; 
tSample     = EES.inputSim.sampleTime;

if length(timeFrame) == 1
    timeFrame = repmat(timeFrame, 1, 2); 
end 

numberOfDaysSimulated = ceil((timeFrame(2)-timeFrame(1))/gvarDAYS2SECONDS); 

% Define index vector with simulation steps
stepVector = (1:EES.stepNow-1);
% Limit index vector to time frame of plotting
stepVector = stepVector(max(timeFrame(1) / tSample,1):max(timeFrame(2) / tSample,1));

switch timeUnit
    case {'years', 'year'}
        timeUnit    = 'years';
        profileTime = stepVector*tSample/gvarYEARS2SECONDS;
        
    case {'days', 'day'}
        timeUnit    = 'days';
        profileTime = stepVector*tSample/gvarDAYS2SECONDS;
        
    case {'hours', 'hour'}
        timeUnit    = 'hours';
        profileTime = stepVector*tSample/3600;
        
    case {'minutes', 'minute'}
        timeUnit    = 'minutes';
        profileTime = stepVector*tSample/60;
        
    case {'seconds', 'second'}
        timeUnit    = 'seconds';
         profileTime = stepVector*tSample;
         
    otherwise 
        disp('plotResults: Chosen timeUnit not possible.')
end

%% Prepare figures
fig1 = figure(figureNo); 
set(fig1, 'Units', 'normalized', 'Position', [0.05 0.05 0.85 0.85]); % Set position of figure relatively to screen size

% This is a 3x2 grid of subplots and the plots are visualized as follows (HM is heat map): 
% |..Storage.....Power..|
% |..State ..of.Charge..|
% |...HMres..|..HMpower.|
% Coded hard
ax(1) = subplot(3,2,[1,2]); 
ax(2) = subplot(3,2,[3,4]); 
ax(3) = subplot(3,2,5); 
ax(4) = subplot(3,2,6); 

%% Plot data
plotStoragePower(EES, ax(1), profileTime, stepVector); % Plot Storage Power
plotSOCandSOH(EES, ax(2), profileTime, stepVector); % Plot SOC and SOH

linkaxes([ax(1) ax(2)], 'x'); 

ax(1).XLim = [profileTime(1) profileTime(end)];
ax(2).XLim = ax(1).XLim; 

xlabel(ax(1), ['Time in ' regexprep(timeUnit,'(\<[a-z])','${upper($1)}')], textstyle{:}); 
xlabel(ax(2), ['Time in ' regexprep(timeUnit,'(\<[a-z])','${upper($1)}')], textstyle{:}); 

plotHeatMap(EES, ax(3), numberOfDaysSimulated, stepVector, 'plotValue', 'SOC'); 
plotHeatMap(EES, ax(4), numberOfDaysSimulated, stepVector, 'plotValue', 'powerBatt');

hold off; 


end

