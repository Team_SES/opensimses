%% plotStoragePower 
% plots the storage power. It is called by plotStorageData()
%
% INPUTS 
%   EES: Storage object (obligatory)
%   axis: axis handle
%   profileTime
%   stepVector
%
% OUTPUTS: h handle
%
% 15.01.2018 Markus F�rstl

function [ h ] = plotStoragePower( EES, axis, profileTime, stepVector)
%% Get default figure settings
run('figureSettingsDefault.m')

if EES.inputSim.simStart == 0
    simStart = 1; 
else 
    simStart = EES.inputSim.simStart; 
end

sampleTime = EES.inputSim.sampleTime; 

hold(axis, 'on')
grid(axis, 'on'); 
box(axis, 'on');

if simStart == 1
    powerBatt       = EES.powerBatt; 
else 
    lengthofVector  = length(EES.powerBatt(simStart/sampleTime:end-1)); 
    powerBatt       = EES.powerBatt(1:lengthofVector); 
end

h(1) = area(axis, profileTime, powerBatt(stepVector), 'EdgeColor', 'none'); 
h(1).FaceColor = [0 0.2 89/255]; 


ylim(axis, [1.05*min(powerBatt(stepVector)), 1.05*max(powerBatt(stepVector))]);
title(axis, 'Storage Power in Watts', textstyle{:});
grid(axis, 'on'); 
hold(axis, 'off'); 

end

