%% plotHistogramSOC 
% plots a histogram of the state of charge of ees object. Fct is called by
% plotAging()
%
% Input: 
%   agingData
%
% 15.01.2018 Markus F�rstl

function [ ] = plotHistogramSOC( ees, ax)

%% Get default figure settings
run('figureSettingsDefault.m')

%% Plot data
hold(ax, 'on');
grid(ax, 'on'); 
box(ax, 'on');

h(1)                = histogram(ax, ees.SOC); 
h(1).Normalization  = 'probability';
h(1).BinWidth       = 0.01;
h.FaceAlpha         = .5; 
h.FaceColor         = colors(1,:);

xlim(ax, [0 1]);
set(ax,'LooseInset',get(gca,'TightInset'))
xlabel(ax, 'State of charge / %', textstyle{:})

xInPercentage = cellstr(num2str((ax.XTick)'*100)); 
new_xTicks = [char(xInPercentage)];
xticklabels(ax, new_xTicks);

ylabel(ax, 'Relative frequency', textstyle{:});
hold(ax, 'off'); 

end

