%% plotSOCandSOH 
% plots the SOC and the SOH of the storage into one plot. Is called by
% plotStorageData()
%
% INPUTS 
%   EES: Storage object (obligatory)
%   axis: axis handle
%   profileTime
%   stepVector
%
% OUTPUTS: none
%
% 15.01.2018 Markus F�rstl

function [ ] = plotSOCandSOH( EES, axis, profileTime, stepVector )
%% Get default figure settings
run('figureSettingsDefault.m')

if EES.inputSim.simStart == 0
    simStart = 1; 
else 
    simStart = EES.inputSim.simStart; 
end

tSample = EES.inputSim.sampleTime; 

if simStart == 1
    SOC = EES.SOC; %Plot it in percent. => Multiplication with 100
    SOH = EES.SOHCapacity;
else 
    lengthofVector  = length(EES.SOC(simStart/tSample:end-1)); 
    SOC             = EES.SOC(1:lengthofVector); 
    SOH             = EES.SOHCapacity(1:lengthofVector);
end

% Conversion p.u. --> %
SOC = SOC * 100; 
SOH = SOH * 100; 

hold(axis, 'on'); 
grid(axis, 'on'); 
box(axis, 'on');

% SOC corresponds to left y-axis
yyaxis(axis, 'left')
h(1)        = plot(axis, profileTime, SOC(stepVector)); 
h(1).Color  = [0 0.2 89/255];
ylim(axis, [0, 105]);

% SOH corresponds to the right y-axis 
yyaxis(axis, 'right')
h(2) = plot(axis, profileTime, SOH(stepVector)); 
ylim(axis, [.95 * min(SOH(stepVector)), 100]);
title(axis, 'State of Charge and State of Health in %',  textstyle{:}); 
l = legend(axis, 'SOC', ['SOH ' num2str(SOH(end)) ' %']); 
set(l, 'Location', 'northwest', textstyle{:});
grid(axis, 'on'); 

end

