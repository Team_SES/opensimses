%% plotHistogramCRateDOC 
% plots a histogram of the C-rate over DOC of ees object. Fct is called by
% plotAging()
% 
% Input:  
%   ees object
%   axis: specified axis in subplot
%   figureSettings: struct for formatting
%
% 15.01.2018 Markus F�rstl

function [ ] = plotHistogramCRateDOC( ees, ax )
%% Get default figure settings
run('figureSettingsDefault.m')

%% Get data
storageSize = ees.inputTech.batteryNominalEnergy / 3600; % Watt hours
[avgCRate, DOC] = calcCRateDOC(ees); 
maxDOC          = ceil(max(abs(DOC)));
nBins           = 10;

%% Plot data
% hold(ax, 'on');
grid(ax, 'on'); 
h                = histogram2(ax, DOC, avgCRate, 'FaceColor', 'flat', 'FaceAlpha', 0.85);
h.Normalization  = 'probability';
colormap(parula);
colorbar

nDecimals       = 1; 
f               = 10.^nDecimals; 
maxCRateRounded = ceil(f*max(abs(ees.powerStorage))/storageSize) / f;  
h.XBinEdges     = [-1:1/nBins:1]; 
h.YBinEdges     = [-maxCRateRounded:maxCRateRounded/nBins:maxCRateRounded];

xlim([-maxDOC maxDOC]); % Depth of Cycle limits
ylim([-maxCRateRounded maxCRateRounded]); % C-Rate limits

xlabel(ax, 'Cycle depth / %', textstyle{:})
xInPercentage = cellstr(num2str((ax.XTick)'*100)); 
new_xTicks = [char(xInPercentage)];
xticklabels(ax, new_xTicks);

ylabel(ax, 'C-rate / h^{-1}',  textstyle{:})
zlabel(ax, 'Relative frequency', textstyle{:});

title(ax, {'Frequency distribution of C-rate over cycle depth'}, textstyle{:}); 
view(ax, [37.5 40]);

hold(ax, 'off'); 

end

