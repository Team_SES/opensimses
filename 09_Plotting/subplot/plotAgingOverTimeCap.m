%% plotAgingOverTimeCap 
% plots the aging of the capacity of the storage into one plot. Fct is
% called by plotAging()
%
% Input:  
%   ees object
%   axis: specified axis in subplot
%   figureSettings: struct for formatting
%
% 15.01.2018 Markus F�rstl

function [ ] = plotAgingOverTimeCap(ees, ax, varargin)
%% Get default figure settings
run('figureSettingsDefault.m')

global gvarYEARS2SECONDS gvarDAYS2SECONDS 

p = inputParser; 
% Default values in case parameter is not set
expectedTimeUnit    = {'years', 'days', 'hours', 'minutes', 'seconds', ...
                        'year', 'day', 'hour', 'minute', 'second'}; % limiting possible time units

defVal = ees.inputSim.plotTimeUnit;
addParameter(p, 'timeUnit', defVal, @(x) any(validatestring(x, expectedTimeUnit))); 

parse(p, varargin{:}); 
timeUnit    = p.Results.timeUnit; 
tSample     = ees.inputSim.sampleTime;
battNomE    = ees.inputTech.batteryNominalEnergy;

stepsBefore = ees.inputSim.simStart / tSample; 
stepVector  = (1:ees.stepNow-1) + stepsBefore; 

switch timeUnit
    case {'years', 'year'}
        timeUnit    = 'years';
        profileTime = stepVector*tSample/gvarYEARS2SECONDS;
        
    case {'days', 'day'}
        timeUnit    = 'days';
        profileTime = stepVector*tSample/gvarDAYS2SECONDS;
        
    case {'hours', 'hour'}
        timeUnit    = 'hours';
        profileTime = stepVector*tSample/3600;
        
    case {'minutes', 'minute'}
        timeUnit    = 'minutes';
        profileTime = stepVector*tSample/60;
        
    case {'seconds', 'second'}
        timeUnit    = 'seconds';
        profileTime = stepVector*tSample;
        
    otherwise 
        disp('plotResults: Chosen timeUnit not possible.')
end

%% Prepare data
changeCapCalRelative = 100 * ees.capacityChangeCalendric  / battNomE; % in percent
changeCapCycRelative = 100 * ees.capacityChangeCyclic     / battNomE;
changeCapTotRelative = 100 * ees.capacityChangeTotal      / battNomE; 

%% Plot figure
hold(ax, 'on'); 
grid(ax, 'on'); 
box(ax, 'on');

% plot capacity loss to the left. 
h(1) = plot(ax, profileTime, -1 * cumsum(changeCapTotRelative), plotstyles{1}{:}, 'Color', colors(1,:)); 
h(2) = plot(ax, profileTime, -1 * cumsum(changeCapCalRelative), plotstyles{2}{:}, 'Color', colors(1,:)); 
h(3) = plot(ax, profileTime, -1 * cumsum(changeCapCycRelative), plotstyles{3}{:}, 'Color', colors(1,:)); 

ax.XLim = [profileTime(1) profileTime(end)]; 
xlabel(ax, ['Time in ' regexprep(timeUnit,'(\<[a-z])','${upper($1)}')], textstyle{:});
ylabel(ax, 'Capacity loss / %', textstyle{:});
l=legend(ax, 'Capacity loss (total)', ...
    'Capacity loss (calendar)',...
    'Capacity loss (cycle)',...
    'Location', 'northwest');
set(l, textstyle{:},'FontSize',fontsize);
hold(ax, 'off'); 

end

