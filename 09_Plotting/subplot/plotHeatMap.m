%% plotHeatMap 
% plots Heat Maps of different properties and is the SO-adapted version of
% plotHeatMap. It is called by plotStorageDataSO()
%
% INPUTS: 
%   EES
%   axis
%   numberOfDaysSimulated
%   plotValue
% 
% OUTPUTS: none
%
% 15.01.2018 Markus F�rstl


function [ ] = plotHeatMap( EES, axis, numberOfDaysSimulated, stepVector, varargin )
%% Get default figure settings
run('figureSettingsDefault.m')

p = inputParser;

defVal              = NaN; 
expectedPlotValue   = {'SOC', 'powerBatt', 'powerGrid'};

addParameter(p, 'plotValue', defVal, @(x) any(validatestring(x,expectedPlotValue))); 
parse(p,varargin{:}); 

if EES.inputSim.simStart == 0
    simStart = 1; 
else 
    simStart = EES.inputSim.simStart; 
end

plotValue   = p.Results.plotValue;
plotProfile = EES.(plotValue)(stepVector); 

if strcmp(plotValue, 'powerGrid')
    plotProfile = -1 * plotProfile; % Power fed into grid is positive
end

%If the length of plotProfile is not dividable by the number of days simulated, it cannot be
%plotted as a heat map (reshape throws an error).
%Then it will be resampled accordingly.
if mod(length(plotProfile), numberOfDaysSimulated)
    scaleFactor             = round(length(plotProfile)/numberOfDaysSimulated); 
    plotProfileResampled    = resample(plotProfile, scaleFactor * numberOfDaysSimulated, length(plotProfile)); 
    plotMatrix              = reshape(plotProfileResampled, [], numberOfDaysSimulated); 
else 
    plotMatrix = reshape(plotProfile, [], numberOfDaysSimulated); 
end
    
entriesPerDay = size(plotMatrix,1); 

axes(axis); 
imagesc(plotMatrix); 
colormap(axis, 'jet'); 
colorbar(axis); 

if numberOfDaysSimulated ~= 1
    xlabel('Days', textstyle{:});
end

ylabel('Hour', textstyle{:});
yTicks = [1 round(entriesPerDay/8) round(entriesPerDay/4) ... %0 3 6 ToD
    round(entriesPerDay/8*3) round(entriesPerDay/2) round(entriesPerDay/8*5) ... % 9 12 15 ToD
    round(entriesPerDay / 4 * 3) round(entriesPerDay/8*7) entriesPerDay ]; % 18 21 24 ToD
yTickLabel = {'0', '3', '6', '9' , '12', '15' , '18', '21', '24'};
set(axis, 'TickLength', [0 0], 'YTick', yTicks, 'YTickLabel', yTickLabel);

title(axis, plotValue, textstyle{:})


end

