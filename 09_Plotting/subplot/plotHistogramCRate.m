%% plotHistogramCRate
% plots a histogram of the C-rate of ees object. Fct is called by
% plotAging()
%
% Input:  
%   ees object
%   axis: specified axis in subplot
%   figureSettings: struct for formatting
%
% 15.01.2018 Markus F�rstl

function [ ] = plotHistogramCRate( ees, ax )
%% Get default figure settings
run('figureSettingsDefault.m')

%% Get data from ees object
powerStorage   = ees.powerStorage; % Watt
storageSize    = ees.inputTech.batteryNominalEnergy / 3600; % Watt hours

% Round maxCRate to first decimal places to adjust limits of x axis
nDecimals   = 1; 
f           = 10.^nDecimals; 
maxCRate    = ceil(f*max(abs(powerStorage))/storageSize(1)) / f;
nBins       = 20;

%% Plot data
hold(ax, 'on'); 
grid(ax, 'on'); 
box(ax, 'on'); 

h                = histogram(ax, powerStorage./storageSize(1)); 
h.BinEdges       = -maxCRate:maxCRate/nBins:maxCRate;
h.Normalization  = 'probability';

h.FaceAlpha      = .5; 
h.FaceColor      = colors(1,:);

xlim(ax, [-maxCRate maxCRate]);     

set(ax,'LooseInset',get(gca,'TightInset'))

xlabel(ax, 'C-rate / h^{-1}', textstyle{:})
ylabel(ax, 'Relative frequency', textstyle{:});

hold(ax, 'off'); 

end

