%% plotResidentialProfile
% plots the power profile of the residential case including storage power.
% One plot in one figure.
%
% INPUTS
%   EES object
%   figureNo
%   timeFrame
%   timeUnit
%
% OUTPUTS: none
%
% USAGE: plotResidentialProfileSO(
%           EES,
%           'figureNo', <number of figures already plotted + 1>, 
%           'timeFrame', <time frame that is plotted>, 
%           'timeUnit', <desired time unit of x axis>)
% 
% 15.01.2018 Markus F�rstl


function [ ] = plotResidentialProfile( EES, varargin )
%% Get default figure settings
run('figureSettingsDefault.m')

global gvarYEARS2SECONDS gvarDAYS2SECONDS 

p = inputParser;

% input check
expectedTimeUnit = {'years', 'days', 'hours', 'minutes', 'seconds', ...
                    'year', 'day', 'hour', 'minute', 'second'}; 

defVal = NaN; 

addParameter(p, 'timeFrame',    defVal); 
addParameter(p, 'timeUnit',     defVal, @(x) any(validatestring(x, expectedTimeUnit))); 
addParameter(p, 'figureNo',     defVal); 

parse(p, varargin{:}); 

timeFrame   = p.Results.timeFrame;
timeUnit    = p.Results.timeUnit;
figureNo    = p.Results.figureNo; 
simParam    = EES.inputSim;
tSample     = EES.inputSim.sampleTime;

% Collection of data that is later plotted. Also adjustment to match the
% desired orientation in the plot
residentialData.load            = EES.inputProfiles.load;
residentialData.generation      = -1 * EES.inputProfiles.generation; 
residentialData.netLoad         = EES.inputProfiles.load - EES.inputProfiles.generation;
% The following three need to be stacked on top of each other in the plot
residentialData.powerGrid       = EES.powerGrid(:);
residentialData.powerStorage    = -1 * EES.powerStorage(:); 
residentialData.powerCurtail    = -1 * EES.powerCurtail(:); 

if length(timeFrame) == 1
    timeFrame = repmat(timeFrame, 1, 2); 
end 

% Define index vector with simulation steps
stepVector = (1:EES.stepNow-1);
% Limit index vector to time frame of plotting
stepVector = stepVector(max(timeFrame(1)/tSample, 1):max(timeFrame(2)/tSample, 1));

switch timeUnit
    case {'years', 'year'}
        timeUnit    = 'years';
        profileTime = stepVector * tSample/gvarYEARS2SECONDS;
        
    case {'days', 'day'}
        timeUnit    = 'days';
        profileTime = stepVector * tSample/gvarDAYS2SECONDS;
        
    case {'hours', 'hour'}
        timeUnit    = 'hours';
        profileTime = stepVector * tSample/3600;
        
    case {'minutes', 'minute'}
        timeUnit    = 'minutes';
        profileTime = stepVector * tSample/60;
        
    case {'seconds', 'second'}
        timeUnit    = 'seconds';
        profileTime = stepVector * tSample;
        
    otherwise 
        disp('plotResults: Chosen timeUnit not possible.')
end

simStart = max(1, simParam.simStart); 

%% Prepare figure
fig1    = figure(figureNo); hold on; grid on; box on;
fields  = fieldnames(residentialData); 

%matrix containing rgb values that are used for coloring the curves
rgbMatrix = [0.4  0.4  0.4; % Consumption
             0.9  0.9    0; % Generation
               0    0    0; % Net Load
               0  0.7    0; % Unfulfilled Power
               0    0  0.9; % Storage Power
             0.8    0    0; % Curtailment
             0.8  0.8  0.8  % Storage Power Operation
             ];
%% Plot data
for k=1:4
    displayName = fields{k}; 
    
    if strcmp(displayName, {'netLoad'}) % Plot net load as a line
        h(k) = plot(profileTime, residentialData.(fields{k})(stepVector), 'LineWidth', 1.25); 
        h(k).Color = rgbMatrix(k,:);
    elseif strcmp(displayName, {'load'}) || strcmp(displayName, {'generation'})
        h(k) = area(profileTime, residentialData.(fields{k})(stepVector), 'EdgeColor', 'none'); 
        h(k).FaceColor = rgbMatrix(k,:);
    else
        h(k:k+2) = area(profileTime, [residentialData.powerGrid(stepVector), residentialData.powerStorage(stepVector), residentialData.powerCurtail(stepVector)], 'EdgeColor', 'none'); 
        h(k).FaceColor = rgbMatrix(k,:); 
        h(k+1).FaceColor = rgbMatrix(k+1,:); 
        h(k+2).FaceColor = rgbMatrix(k+2,:);
    end

end

l = legend(h,'Consumption','Generation','Net Load','Power to Grid','Storage Power','Curtailment Losses','Location','NorthWest');
set(l, 'Location', 'NorthEast', textstyle{:})

xlim([profileTime(1) profileTime(end)]);
xlabel(['Time in ' regexprep(timeUnit,'(\<[a-z])','${upper($1)}')], textstyle{:}); 
ylabel('Power in Watts', textstyle{:});
set(gca,'YDir','reverse');

hold off;


end

