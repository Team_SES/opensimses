%% Residential
%
%   Class definition for object with generation, consumption and storage
%   device. Based on EES residential consumer with PV-unit and energy storage system.
%   Uses system object storage for computation. Additional fields to model
%   the system with PV and load. Profiles are included in object.
%   Equivalent to building simulink model.
%
%   2018-01-05 Naumann/Truong
%%
classdef residential < storage
    
    %%% Properties and arrays to save history of states etc.
    properties (GetAccess = public, SetAccess = protected, Hidden = false)
        %% Input data
        inputProfiles       % load and generation profile stored in object
        inputForecast       % forecast of profiles stored in object and changeable
        
        %% Properties
        powerGrid                   % [W] power drawn from grid
        powerCurtail                % [W] curtailed power
        resultsTechnical            % struct with technical results
        inputEconomics              % struct with economic parameters
        resultsEconomics            % struct with economics results
    end

    %%% public methods
    methods
        % constructor
        function EES = residential( varargin )
                        
            %% parse input and check if input vars are complete
            p = inputParser;
            defVal = NaN;
            
            addParameter(p, 'inputSim',     defVal);
            addParameter(p, 'inputTech',    defVal);
            addParameter(p, 'inputProfiles',     defVal);
            addParameter(p, 'inputForecast',     defVal);
            
            parse(p,varargin{:});

            inputSim        = p.Results.inputSim;
            inputTech       = p.Results.inputTech;
            inputProfiles   = p.Results.inputProfiles;
            inputForecast   = p.Results.inputForecast;
            
            %% call constructor and build object in superclass 
            EES@storage(... 
                            'inputSim',     inputSim,...
                            'inputTech',    inputTech);
            
            %% set class specific properties
            setPropertiesResidential(EES, ...
                            'inputProfiles', inputProfiles, ...
                            'inputForecast', inputForecast);
            
        end
        
        % methods to change forecast ofter creation of object
        function EES = setLoadForecast( EES, loadFC ) 
            EES.inputForecast.load       = loadFC;   % set new forecast profile
        end
        function EES = setGenerationForecast( EES, genFC ) 
            EES.inputForecast.generation = genFC;    % set new forecast profile
        end
        
        % methods to hand over input for economic evaluations
        function EES = setInputEconomics( EES, inputEconomics ) 
            EES.inputEconomics       = inputEconomics;   % set new inputEconomics
        end
        
        %% Declaration of the methods in separate files     
        [ EES ] = runStorage( EES )
        [ EES ] = evalTechnicalResidential( EES )
        [ EES ] = evalEconomics( EES, varargin )
    end
    
    %%% protected methods
    methods(Access = protected)
        
        function setPropertiesResidential(EES, varargin )
            
            %% parse input and check if input vars are complete
            p = inputParser;
            defVal = NaN;
            
            addParameter(p, 'inputProfiles',     defVal);
            addParameter(p, 'inputForecast',     defVal);
            
            parse(p,varargin{:});
            
            EES.inputProfiles   = p.Results.inputProfiles;
            EES.inputForecast   = p.Results.inputForecast;
        end
    end % methods
end % classdef