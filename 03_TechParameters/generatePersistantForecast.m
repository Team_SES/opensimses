%% generatePersistantForecast
% Creates forecast profiles for given profile with persistance method.
% Actual profile is shifted. E.g. previous day or week is used as forecast.
%
% Input == (parameters)
% profile           [W]     input profile for shifting
% persistancePeriod [s]     persistance period for shifting (e.g. day before or week before)
% sampleTime        [s]     sampleTime of input profile
%
%
% Output ==
% forecastProfile   [W]     array of generated forecast profile
%
%
% Profile is shifted to given persistancePeriod. Actual profile of previous 
% time is used as forecast.
%
% 2015-12-11 Nam Truong
%
%%

function [ forecastProfile ] = generatePersistantForecast( varargin )
%% parse input parameters
p       = inputParser;  % parser handle
defVal  = NaN;          % default value for parsing
% add parameters accepted for input
addParameter(p, 'persistancePeriod',    defVal)
addParameter(p, 'profile',              defVal)
addParameter(p, 'sampleTime',           defVal)
% parse input
parse(p, varargin{:})
% write parsed input into local var
persistancePeriod   = p.Results.persistancePeriod;
profile             = p.Results.profile;
sampleTime          = p.Results.sampleTime;

%% usage of circshift to generate forecast
% calculate required shifts to meet requested time period
cirshiftSteps       = round(persistancePeriod/sampleTime);
% generate persistant forecast 
forecastProfile     = circshift(profile(:), cirshiftSteps, 1);

end
