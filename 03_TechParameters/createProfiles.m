%% createProfiles
% Scales given profiles for simulation of simSES Object. Either energy of
% profile or peak value is adapted.
%
% Input == (parameters)
% profile       [W]     input profile to be scaled
% profileEnergy [Ws]    desired energy of power profile during profile period
% profilePeak   [W]     desired peak value of profile
% profilePeriod [s]     time length of input profile
% simPeriod     [s]     time period of simulation
% sampleTime    [s]     sample time for simulation
%
% 
% Output == 
% profile       [W]     profile of profile used for simulation
%
%
% Profiles are created based on technical input data. Profiles are
% replicated or shortened to match simulation period. 
% Scaling to desired peakpower or profile energy within profile period
% either peakPower or profileEnergy can be set, not both.
%
% 2 optional methods are possible for profile scaling:
%   1. scaling of peak of profile to given peak power
%   2. scaling of profile to match profile's sum to given profileEnergy.
%   Only time period of profile is considered for this calculation.
%
%
% 2015-12-21 Nam Truong    
%
%%

function [ profile ] = createProfiles( varargin )
%% parse input parameters
p       = inputParser;  % parser handle
defVal  = NaN;          % default value for parsing
% add parameters accepted for input
addParameter(p, 'profile',          defVal)
addParameter(p, 'profileEnergy',    defVal)
addParameter(p, 'profilePeak',      defVal)
addParameter(p, 'profilePeriod',    defVal)
addParameter(p, 'simPeriod',        defVal)
addParameter(p, 'sampleTime',       defVal)
% parse input
parse(p, varargin{:})
% write parsed input into local var
inputProfile    = p.Results.profile;
profileEnergy   = p.Results.profileEnergy;
profilePeak     = p.Results.profilePeak;
profilePeriod   = p.Results.profilePeriod;
simPeriod       = p.Results.simPeriod;
sampleTime      = p.Results.sampleTime;

%% scaling of profile
% generation of x-vector
outputProfileLength = floor(linspace(1, length(inputProfile),(profilePeriod/sampleTime)));

% sampling of loadProfile
profile_x       = 1:length(inputProfile);
profile         = interp1(profile_x, inputProfile, outputProfileLength);

%% Determine task of function
% if neither profilePeak or profileEnergy is set, profile is just adapted
% to simulation period and sample time
if any(strcmp(p.UsingDefaults,'profilePeak')) && any(strcmp(p.UsingDefaults,'profileEnergy'))
% if 'profileEnergy' is set, profile is scaled until sum of profile matches 
% profileEnergy. profilePeriod is regarded to match that sum.
elseif any(strcmp(p.UsingDefaults,'profilePeak'))
    profile     = profile* profileEnergy/(sum(profile)*sampleTime); 
% if 'peakPower' is set, profile is scaled to match peak power
elseif any(strcmp(p.UsingDefaults,'profileEnergy'))
    profile     = profilePeak / max(profile) * profile;     
% of both 'peakPower' and 'profileEnergy' are set --> unclear calculation
else
    error([mfilename('fullpath') ': Ambiguous parameter specification. Profile energy and peak power cannot be set at the same time.'])
end

% create length and cut to desired simulation period
profile     = repmat(profile(:), ceil(simPeriod(2)/profilePeriod), 1);
profile     = profile(floor(simPeriod(1)/sampleTime)+1:(simPeriod(2)/sampleTime));

    

end

