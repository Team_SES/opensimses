%% createPowerElectronicsData
% Returns power electronics Data according to chosen model of efficiency.
%
% Input == (parameters)
% powerElectronicsMethod    [-]     string for switch case to choose method to generate efficiency curve
% powerElectronicsP_0       [-]     parameter for curve equation
% powerElectronicsK         [-]     parameter for curve equation
% powerEletronicsEta        [pu]    parameter for constant method
%
% Output ==
% powerElectronicsEta       [pu]    efficiency curve / value
%
% Efficiency curve of power electronics / inverter is generated in function
% according to chosen method 'Formula', 'Array', or 'constant'. Either
% literature equation is used to generate curve or array in function.
% Alternatively constant efficiency value is used for constant method.
%
% 2015-10-12  Maik Naumann
%
%%

function [powerElectronicsEta] = createPowerElectronicsData( varargin )
%% input parsing
p       = inputParser;  % generate parsing handle
defVal  = NaN;          % set def value for parsing
% add parameter accepted for input
addParameter(p ,  'powerElectronicsMethod',     defVal  );
addParameter(p ,  'powerElectronicsP_0',        defVal  );
addParameter(p ,  'powerElectronicsK',          defVal  );
addParameter(p ,  'powerElectronicsEta',        defVal  );
% parse input
parse(p, varargin{:})
% write parsed input into local var
powerElectronicsMethod  = p.Results.powerElectronicsMethod;
powerElectronicsP0      = p.Results.powerElectronicsP_0;
powerElectronicsK       = p.Results.powerElectronicsK;
powerElectronicsEta     = p.Results.powerElectronicsEta;


%% Switch case to determine method for generating efficiency curve
switch lower( powerElectronicsMethod )
    
    %% Formula
    % Inverter efficiency according to given function.
    %
    %   Notton, G.; Lazarov, V.; Stoyanov, L. (2010): Optimal sizing of 
    %       a grid-connected PV system for various PV module technologies 
    %       and inclinations, inverter efficiency characteristics and 
    %       locations. In: Renewable Energy 35 (2), S. 541�554. 
    %       DOI: 10.1016/j.renene.2009.07.013.
    case('formula')
        p                       = 0:0.001:1;
        powerElectronicsEta     = p./ (p + powerElectronicsP0 + powerElectronicsK * p.^2);
    
        
    %% constant
    % Input eta is transformed to mean value.
    case('constant')
        powerElectronicsEta = mean(powerElectronicsEta);
        
        
    %% default
    % error is given for non-existing choice of method
    otherwise
        error([mfilename('fullpath') ': No power electronics efficiency method specified. Please choose method to determine inverter efficiency.'])
        
        
end % end switch

disp([mfilename('fullpath') ':'])
disp(['<strong> Power electronic parameters: ', powerElectronicsMethod, '</strong>'])

end % end function
