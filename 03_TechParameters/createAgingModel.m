%% createAgingModel
% Function to return aging model struct during parameter-setting phase of
% main simulation. Struct will be used to instantiate storage object and
% the aging model parameters are determined.
%
% agingModel = createAgingModel('param','value')
%
% Input == (parameters)
% agingModelType    [-]     string with chosen aging model
%
% Output ==
% agingModel        [-]     Returns aging model of choosen technology:
%   # Lib_Rosenkranz
%   # CLFP_Goebel
%   # NMC_Tesla_DailyCyclePowerwall
%   # CLFP_Sony_US26650_Experiment
%   # NoAging
%   # Dummy
%
% Switch case below is chosen with input parameter and the according
% parameters are set.
%
% 2017-08-08   Maik Naumann
%
%%

function [ agingModel, combAgingFct ] = createAgingModel( varargin )

%% Input parsing
p = inputParser;    % generate parsing handle
defVal = NaN;       % set def value for parsing
% add parameter accepted for input
addParameter(p, 'agingModelType', defVal);
% parse input
parse(p, varargin{:})
% write parsed input into local var
agingModelType     = p.Results.agingModelType;

%% Configuration values for stress characterization 
agingModel.remainCapacityEOL    = 0.8;  % If % of E_N is reached SoH = 0% -> Battery replacement
agingModel.DOCThresh            = 0.01; % Threshold for the minimum cycle depth that is considered with the aging model

% Empty struct fields to avoid errors in later functions (parser)
agingModel.calAgingCapFct  = [];
agingModel.calAgingResFct  = [];
agingModel.cycAgingCapFct  = [];
agingModel.cycAgingResFct  = [];

%% Switch case for aging function selection
switch lower(agingModelType)
    %% LiB_Rosenkranz
    % Weak aging case for paper:
    %
    %   Naumann, Maik; Truong, Cong Nam; Karl, Ralph Ch; Jossen, Andreas; 
    %       Energiespeichertechnik, Elektrische (2014): Betriebsabhängige 
    %       kostenberechnung von energiespeichern. In: 13. Symposium 
    %       Energieinnovation. Graz, S. 1–16.
    case('lib_rosenkranz') 
        agingModel.remainCapacityEOL        = 0.8;                          % If 80% of E_N is reached SoH = 0% -> Battery replacement
        agingModel.DOCThresh                = 0.01;                         % Threshold for the minimum cycle depth that is considered with the aging model
        % set calendric and cyclic aging models (fhandles)
        agingModel.calAgingFct  = @LiB_Rosenkranz_CalAging;
        agingModel.cycAgingFct  = @LiB_Rosenkranz_CycAging;
        
        combAgingFct            = @combAgingType_Superposition;             % Function for combination of battery aging factors 
        
        
    %% CLFP_Goebel
    % Aging data and model according to paper:
    %
    %   Goebel, C.; Hesse, H. et al. (2016): Model-based Dispatch Strategies 
    %       for Lithium-Ion Battery Energy Storage applied to Pay-as-Bid 
    %       Markets for Secondary Reserve. In: IEEE Trans. Power Syst., 
    %       S. 1. DOI: 10.1109/TPWRS.2016.2626392.
    case('clfp_goebel') 
        agingModel.remainCapacityEOL    = 0.8;                              % If 80% of E_N is reached SoH = 0% -> Battery replacement
         agingModel.DOCThresh           = 0.01;                             % Threshold for the minimum cycle depth that is considered with the aging model
        % set calendric and cyclic aging models (fhandles)
        agingModel.calAgingFct  = @CLFP_Goebel_CalAging;
        agingModel.cycAgingFct  = @CLFP_Goebel_CycAging;
        
        combAgingFct            = @combAgingType_Maximum;                   % Function for combination of battery aging factors 

        
    %% NMC_Tesla_DailyCyclePowerwall
    % NMC_Tesla_DailyCyclePowerwall aging data is fitted on data of Tesla 
    % DailyCycle Powerwall warranty sheet.
    case('nmc_tesla_dailycyclepowerwall')      
        agingModel.remainCapacityEOL        = 0.6;                          % If 80% of E_N is reached SoH = 0% -> Battery replacement
        agingModel.DOCThresh                = 0.01;                         % Threshold for the minimum cycle depth that is considered with the aging model
        
        % set calendric and cyclic aging models (fhandles)
        agingModel.calAgingFct  = @NMC_Tesla_DailyCyclePowerwall_CalAging;
        agingModel.cycAgingFct  = @NMC_Tesla_DailyCyclePowerwall_CycAging; 
        
        combAgingFct            = @combAgingType_CyclicOnly;                % Function for combination of battery aging factors 

        
    %% NoAging
    % Case for simulations without aging (improved speed)
    case('noaging')
        agingModel.remainCapacityEOL    = 0.6;                              % If 80% of E_N is reached SoH = 0% -> Battery replacement
        agingModel.DOCThresh            = 0.01;                             % Threshold for the minimum cycle depth that is considered with the aging model
        % set calendric and cyclic aging models (fhandles)
        agingModel.calAgingFct  = @noAgingFct;
        agingModel.cycAgingFct  = @noAgingFct;
        
        combAgingFct            = @combAgingType_noAging;                   % Function for combination of battery aging factors 
        
        
    %% default
    % error for invalid input
    otherwise
        error([mfilename('fullpath') ': No battery aging model specified.'])
end

disp([mfilename('fullpath') ':'])
disp(['<strong> Aging model: ', agingModelType, '</strong>'])

end

