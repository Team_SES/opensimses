%% analyseResultsGUI.m
%
% Script that calls plot functions and evaluating functions. Is called by
% GUI 

if ~isempty(app.eesFromEarlierSimulation)
    ees = app.eesFromEarlierSimulation;
    gvarYEARS2SECONDS = 365 * 24 * 3600;
else
    disp('No ees object found'); 
    return; 
end

% Determine how many figures are already open
h =  findobj('type','figure');
figuresOpened = length(h);

if app.PlotStorageDataCheckBox.Value
    plotStorageData( ees, 'figureNo',       figuresOpened + 1, ...
                          'timeFrame',    [ees.inputSim.plotFrom ees.inputSim.plotTo],    ...
                          'timeUnit',     ees.inputSim.plotTimeUnit); 
                    
    figuresOpened = figuresOpened + 1;
end

if app.PlotPowerProfilesCheckBox.Value
    plotResidentialProfile( ees, 'figureNo', figuresOpened + 1, ...
                                 'timeFrame',    [ees.inputSim.plotFrom ees.inputSim.plotTo], ...
                                 'timeUnit',     ees.inputSim.plotTimeUnit);
                    
	figuresOpened = figuresOpened + 1;
end

if app.PlotStorageAgingDataCheckBox.Value
    % Plotting of aging results when logging values are available
    if(ees.inputSim.logAgingResults && ~strcmp(ees.inputTech.agingModelType, 'noaging'))  
        plotAging( ees, 'figureNo', figuresOpened + 1, 'timeUnit', ees.inputSim.plotTimeUnit, 'scaleYAxis', 'log');
        figuresOpened = figuresOpened + 1;
    else
        disp('Plotting of aging results not possible because no aging values are available');
    end   
end

if app.AnalyzeEconomicsCheckBox.Value
    run('evaluateEconomics_GUI.m');
end