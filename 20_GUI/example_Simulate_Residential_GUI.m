%% Example simulate residential
% Example script to start simulation of residential storage. Calling of
% subclass _residential_ for specific application of increasing a
% prosumer's self-consumption.
% 
%%
% clear
close all
%% Preparing MATLAB
% Setting up Matlab search path and global helping variables.

% Global helping variables for conversion
global gvarYEARS2SECONDS gvarDAYS2SECONDS gvarKWH2WS
gvarYEARS2SECONDS   = 3600 * 24 * 365;  % helping variable to convert between years and seconds
gvarDAYS2SECONDS    = 3600 * 24;        % helping variable to convert between days and seconds
gvarKWH2WS          = 3600e3;           % helping variable to convert between kWh and Ws

% Run script to add required subfolders to MATLAB's search path. 
run('addRequiredPaths.m')   

%% Input profiles
% Load chosen profile data into workspace. Used function checks for errors.
% If given load profiles do not exist in folder, please download from
% SimSES Bitbucket download area before or during the function call.
localFilesFolder    = '01_ProfileData';
generationProfile   = returnInputProfile(...
                        'profileFileName',      'PV_EEN_Power_Munich_2014.mat', ...
                        'profileServerPath',    'https://bitbucket.org/Team_SES/opensimses/downloads/', ...
                        'localFilesFolder',     localFilesFolder, ...
                        'verifyHashCode',       false);
                    
loadProfile         = returnInputProfile(...
                        'profileFileName',      'EEN_100_RES_Load_Profiles_1min_1a.mat', ...
                        'profileServerPath',    'https://bitbucket.org/Team_SES/opensimses/downloads/', ...
                        'localFilesFolder',     localFilesFolder, ...
                        'verifyHashCode',       false, ...
                        'profileNumber',        1);
                    
%% Configuration of simulation
% Setting of necessary input parameters. Structs hold parameters with
% according fieldnames. Struct fieldnames cannot be changed.
%
% * inputSim
% * inputTech
% * inputEconomics

%% Simulation parameters
% Simulation parameters _inputSim_. Sample time, simulated time and logging
% flags are set here.

% Get parameters from GUI
inputSim = app.inputSimGUI; 

% Set everything else 
inputSim.simStart           = 0; 
inputSim.loadProfileLength  = 1 * gvarYEARS2SECONDS;        % [s] length of input load profile 
inputSim.genProfileLength   = 1 * gvarYEARS2SECONDS;        % [s] length of input generation profile 

inputSim.plotFrom           = inputSim.simStart;            % [s] starting time of plot 
inputSim.plotTo             = inputSim.simEnd;              % [s] last time to be included in plot 

inputSim.saveResults        = false;                        % TRUE: the variables will be saved in a .mat-file
inputSim.logBatteryEcOutput = false;                        % TRUE: all output values of equivalent circuit (EC) model are logged
inputSim.logAgingResults    = true;                         % TRUE: all output values of _agingStress_ are logged


%% Technical parameters
% Technical parameters _inputTech_. Check fieldnames of variable to set
% desired desired values.

% Use parameters from GUI
inputTech = app.inputTechGUI; 

%% Include everything that is NOT set within GUI (Technical)
% Load and SOH
inputTech.persistancePeriodLoad     = 0;                    % [s] time period of shifting load profiles (persistant forecast)
inputTech.persistancePeriodPV       = 0;                    % [s] time period of shifting load profiles (persistant forecast)
inputTech.sohResistanceStart        = 1;                    % [pu] initial SOH of storage resistance

% Parameters for stress detection method
inputTech.stepSizeStressCharacterization    = 10;            % [~] Step size for calling of stress characterization
inputTech.stepSizeCalendarAging             = 10;            % [~] Step size for calling of calendar aging model 
inputTech.stepSizeCycleAging                = 10;            % [~] Step size for calling of cycle aging model (should be smaller or equal to stepSizeStressCharacterization)

% Aging
inputTech.callMethodAgingModels     = @callMethodAgingModels_AverageValues;  % Function for the different method and strategies of calling the aging calculation -> see stepsToStartAgingFct

% Thermal Model 
inputTech.thermalModelFunction      = @fThermalCell;

% Power Electronics
inputTech.etaAccuracy               = 100;
inputTech.powerElectronicsK         = 0.0345;               % parameter for 'Formula'
inputTech.powerElectronicsP_0       = 0.0072;               % parameter for 'Formula'
inputTech.powerStorageOp            = 0;                    % [W] power consumption required to operate storage device (sensors, controller, thermal management)                 

% Get replacement data for setReplacement method
[inputTech.replacementData]         = createBatteryReplacementData('Generic'); % Struct with replacement data
% Set specific replacement interval or schedule next replacement
inputTech.scheduledNextReplacement  = 0; % [s] Absolute time for first storage replacement after simulation start. Set to zero if no replacement is desired.
inputTech.replacementInterval       = 0; % [s] Time interval for storage replacements after first replacement. Set to zero if no replacement is desired.

%% Creating advanced parameter
% Run script that uses pre-determined input parameters to generate more
% complicated input data (price development, efficiency curves, ...)
run('createTechParamPVHomeStorage.m')

% Set End of Life at the end to not affect original createAgingModel
% function which sets EOL according to specified agingModelType.
inputTech.agingModel.remainCapacityEOL = app.inputTechGUI.agingModel.remainCapacityEOL;

%% Generate object and run simulation
% Call class constructor method to generate object. Constructor is executed
% with parameter value pair method of MATLAB.
ees = residential(  'inputSim',         inputSim,       ...
                    'inputTech',        inputTech,      ...
                    'inputProfiles',    inputProfiles,  ...
                    'inputForecast',    inputForecast   );
                
%%
% Run simulation with generated object.
disp('Start Matlab Simulation')         % Display start of simulation at command window 
tic   
ees = runStorage( ees );                      % call run storage method for simulation run
toc
disp('Simulation complete'); 

%% Evaluation and analysis of results
% Call evaluation functions for analysis of simulation.
disp('Evaluating:')
ees = evalTechnicalResidential( ees );  % calculate technical assessment values

%% Plotting and saving of data
% Plotting of profiles (power flows, SOC, SOH) for quick analysis.
disp('Plotting.')
plotStorageData( ees, 'figureNo',       1,                                      ...
                        'timeFrame',    [inputSim.plotFrom inputSim.plotTo],    ...
                        'timeUnit',     inputSim.plotTimeUnit                   ); 
                    
plotResidentialProfile( ees, 'figureNo', 2,                                      ...
                        'timeFrame',    [inputSim.plotFrom inputSim.plotTo],    ...
                        'timeUnit',     inputSim.plotTimeUnit                   );
                    
% Plotting of aging results when logging values are available
if(inputSim.logAgingResults && ~strcmp(inputTech.agingModelType, 'noaging'))
    plotAging( ees, 'figureNo', 3, 'timeUnit', ees.inputSim.plotTimeUnit, 'scaleYAxis', 'log');
end

%% Economic evaluation
run('evaluateEconomics_GUI.m'); 

%%
% Saving workspace variables.
% Save ees object.
if inputSim.saveResults == 1
    save(['07_Results\EES_',num2str(inputTech.batteryNominalEnergy(k)/3600e3),'kWh_',datestr(date),'.mat'], 'ees');
    disp('Results saved.') 
end

% Since the script is called by GUI the workspace variables would be
% deleted after the script is finished (different workspace). Hence the
% desired variables are stored in the base workspace
assignin('base', 'ees', ees);
assignin('base', 'inputSim', inputSim);
assignin('base', 'inputTech', inputTech);
assignin('base', 'inputEconomics', inputEconomics);
assignin('base', 'inputProfiles', inputProfiles);
assignin('base', 'inputForecast', inputForecast);

assignin('base', 'loadProfile', loadProfile);
assignin('base', 'generationProfile', generationProfile);

disp('Finished.')