%% Economic calculations
% After technical simulation, economic assumptions can be modified and the
% system's economic value assessed for the specific application.

%% Economic parameters
% Generate economic parameters .

% generates standard struct for required economic parameters:
%   electricity price       increase for electricity price based on extrapolation of 2004-2017 price: BDEW, Bundesnetzagentur for household with 3.500 kWh/year
%   storage price           average LFP price according to market (Q1 17)
%   subsidy rate            0.22 p.u. of system price subsidized
%   feed in remuneration    12.56 ct/kWh
%   interest rate           0.04 p.u.
%   inflation rate          0.02 p.u.
%   depreciation perediod   20 a
%   installation cost       0 p.u. of system price


%%
if ~isempty(app.eesFromEarlierSimulation)
    ees = app.eesFromEarlierSimulation;
    gvarYEARS2SECONDS = 365 * 24 * 3600;
end

% Get economics data from GUI
inputEconomics = app.inputEconomicsGUI;

%%
% Create cost for storage system. => this is handled in the GUI now
% inputEconomics = createStorageCosts( inputEconomics ); 

%%
% create application specific economic parameters 
inputEconomics = createElectricityPrices( inputEconomics );
inputEconomics = createFeedInRemuneration( inputEconomics );

%% Calculate economic results
% Calculate economic key figures (LCOE, NPV, ...) with method of
% residential class.
ees = evalEconomics( ees, inputEconomics );


% save inputEconomics and object to worksapce 
if ~isempty(app.eesFromEarlierSimulation)
    assignin('base', 'inputEconomics', inputEconomics);
    assignin('base', 'ees', ees); 
end


disp(ees.resultsEconomics)

