# README #

SimSES (Simulation of stationary energy storage systems) is an open source modeling framework for simulating stationary energy storage systems.

The tool has been developed in MATLAB, mainly by Maik Naumann and Nam Truong at the [Institute for Electrical Energy Storage Technology - EES](http://www.ees.ei.tum.de/)

For more information and comments, please contact [simses.ees@ei.tum.de](mailto:simses.ees@ei.tum.de)

News about major changes or new developments can be received by subscribing to our mailing list: [SimSES.News](https://lists.lrz.de/mailman/listinfo/simses.news)

## About SimSES ##

SimSES aims to provide a modeling framework from a macroscopic system point of view including inverter efficiency curves.
Yet aging of battery cells is considered because of its impact on the overall system performance.

The tool has been developed in the context of battery storage systems, but is easily extendable for other energy storage systems.
All system components can be interchanged with different model types.

The computations regarding grid and converter is realized as power-flow calculations with according efficiency curves. 
The battery is modeled either as power flow model or as equivalent circuit R-model.
Battery cell aging is not modeled as physico-chemical process (yet), but implemented with more abstract cycle-counting algorithms that determine the stress on the battery.

The model depth has been chosen to achieve sufficient accuracy for techno-economic consideration and at the same time allow fast simulations, even for decades of simulated time periods.
Battery degradation, converter and battery efficiency, control algorithms, as well as system sizing are considered in the model framework.

Application specific evaluations and mechanisms can be added seamelessly by creating subclasses of the provided class definition.

The tool has been used for several publications.

### Economic evaluations ###
* Naumann, M.; Karl, R.Ch.; Truong, C.N.; Jossen, A.; Hesse, H.C. (2015): Lithium-ion Battery Cost Analysis in PV-household Application. In: Energy Procedia 73, S. 37–47. DOI: [10.1016/j.egypro.2015.07.555](https://doi.org/10.1016/j.egypro.2015.07.555)
* Truong, C.; Naumann, M.; Karl, R.; Müller, M.; Jossen, A.; Hesse, H. (2016): Economics of Residential Photovoltaic Battery Systems in Germany. The Case of Tesla’s Powerwall. In: Batteries 2 (2), S. 14–30. DOI: [10.3390/batteries2020014](http://dx.doi.org/10.3390/batteries2020014)

### Reference for other methods ###
* Hesse, H.C.; Martins, R.; Musilek, P.; Naumann, M.; Truong, C.N.; Jossen, A.	Economic Optimization of Component Sizing for Residential Battery Storage Systems. Energies 2017, 10, 835.

## Requirements ##

The code requires MATLAB to run and has been tested in MATLAB 2016a, 2016b, 2017a and 2017b.
It should work with older MATLAB versions as well, but has not been tested.

## Getting Started ##

Use Git to checkout the Sourcecode (e.g. with Sourcetree).
A comprehensive manual for Git, Sourcetree and Bitbucket can be found [here](https://confluence.atlassian.com/get-started-with-bitbucket/get-started-with-bitbucket-cloud-856845168.html).

After obtaining the files, we recommend starting with the example script (example_Simulate_Residential.m) in the root folder.
The example script shows how to use SimSES and automatically triggers downloads the required files for photovoltaic and load profiles.
Refer to [this link](https://bitbucket.org/Team_SES/opensimses/downloads/example_Simulate_Residential.html) for the description of the example script.

## Contribution guidelines ##

If you use SimSES in your work, please cite our paper [Maik Naumann, ETG Paper](https://www.ees.ei.tum.de/fileadmin/w00bee/www/SimSES/SimSES_Paper.pdf) and the code using its DOI: [10.14459/2017mp1401541](http://doi.org/10.14459/2017mp1401541)

* Naumann, M; Truong, C.N.; Schimpe, M. ; Kucevic, D.; Jossen, A.; Hesse, H.C. (2017): SimSES: Software for techno-economic Simulation of Stationary Energy Storage Systems. In: VDE-ETG-Kongress 2017. Bonn. Preprint accepted for publication in IEEE Conference Proceedings

## License ##

This open-source MATLAB code is published under the BSD 3-clause License, please read the [LICENSE.txt](https://bitbucket.org/Team_SES/opensimses/downloads/license.txt) file for more information.
