%% OSGreedy
% OSGreedy function handle for operation strategy 'greedy' in application
% to increase self-consumption of system (e.g. residential home storage).
% Basically load shifting as task.
%
%
% Input ==
% EES   [-] storage object for OS
%
%
% Output ==
% EES   [-] storage object for OS
%
%
% Strategy is for the BESS to offset the residual load = load - generation.
% Excess power is stored in BESS and excess demand is supplied by BESS. 
% No forecast or consideration of system limits.
% To be called in runStorage method of residential class.
%
%  2017-07-27 Nam Truong 
%%

function [ EES ] = OSPVHomeGreedy( EES )
%% object data --> local vars
sampleTime      = EES.inputSim.sampleTime;        
simEnd          = EES.inputSim.simEnd;
simStart        = EES.inputSim.simStart;
load            = EES.inputProfiles.load;
generation      = EES.inputProfiles.generation;

%% pre-calculations for simulation 
stepsPerDay     = 24 * 3600 / sampleTime; 
dayStart        = max(simStart/(3600*24),1);
dayEnd          = simEnd/(3600*24);
simTime         = (simStart:sampleTime:simEnd);
simTime         = simTime(2:end);

%% operation strategy ===
netLoad         = load - generation;    % compute net load
pRef            = - netLoad;            % reference is to offset net load

% loops to iterate through each timestep 
for dayi = dayStart:dayEnd                                                  % loop for each day
    steps = ( ( dayi-1 ) * stepsPerDay + 1 ) : ( ( dayi ) * stepsPerDay );  % obtain steps of the day
    
    for step = steps                                                        % loop for each timestep
        EES.step( pRef(step), simTime(step) );                                   % run step
    end % end step
end % end dayi

end % end function

