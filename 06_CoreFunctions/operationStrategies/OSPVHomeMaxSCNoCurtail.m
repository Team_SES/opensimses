%% OS Self Consumption Optimization avoiding Curtail
% OS for residential case to maximize the self-consumption while avoiding
% curtailment losses.
%
%
% No forecast as input because OS is only intended to be reference for
% maximum self-consumption by avoiding curtailment.
% Expected curtailed energy is calculated and computed to corresponding SOC
% of ESS. SOC limit is temporarily changed to keep sufficient storing
% capacity to store the power above curtailment limit.
%
% 2017-08-01 Truong
%
%%

function [ EES ] = OSPVHomeMaxSCNoCurtail( EES, varargin )
%% object data --> local vars
sampleTime      = EES.inputSim.sampleTime;          
simEnd          = EES.inputSim.simEnd;
simStart        = EES.inputSim.simStart;
load            = EES.inputProfiles.load;
generation      = EES.inputProfiles.generation;

%% pre-calculations for simulation 
stepsPerDay     = 24 * 3600 / sampleTime; 
dayStart        = max(simStart/(3600*24),1);
dayEnd          = simEnd/(3600*24);
simTime         = (simStart:sampleTime:simEnd);
simTime         = simTime(2:end);
%% 2017-08-02 NT: estimation of 92% one way efficiency! better way to compute limit required!
avgEta          = 0.92; 

%% operation strategy ===
netLoad         = load - generation;    % compute net load
pRef            = - netLoad;            % compute reference power to offset net load

% loops to iterate through each time step
for dayi = dayStart:dayEnd                                                  % loop for each day
    steps = ( ( dayi-1 ) * stepsPerDay + 1 ) : ( ( dayi ) * stepsPerDay );  % obtain steps of the day
    % calculate curtailed power
    powerCurtail    = min(netLoad(steps) - EES.inputTech.power2GridMax(1), 0);

    %% if curtailment is expected during the day, change SOC limit accordingly
    if any(powerCurtail) 
        % calculate power into energy and obtain according SOC limit for each timestep
        energyCurtail   = - cumsum(powerCurtail * EES.inputSim.sampleTime); % compute the cumulated curtailed energy over the day
        energyReserve   = ( max(energyCurtail) - energyCurtail ) * avgEta;  % compute required energy to store the comulated curtailed energy
        % compute required momentary SOC limit to keep energy reserve of battery
        SOCmax          = max(EES.inputTech.SOCLimHigh - energyReserve/EES.inputTech.batteryNominalEnergy, EES.inputTech.SOCLimLow);  
        
        for step = steps % day loop
            % set limit for EES to prevent premature full state
            SOCLimHigh = max( SOCmax( step - ( dayi - 1 ) * stepsPerDay ), EES.SOCNow );
            SOCLimHigh = min( EES.inputTech.SOCLimHigh, SOCLimHigh );
            setSOCLim( EES, [EES.inputTech.SOCLimLow, SOCLimHigh] );
            % sim step
            EES.step( pRef( step ), simTime( step ) );
        end
        resetSOCLim( EES ); % restore SOC-limit

    %% if no curtailment is expected that day: greedyOS
    else
        for step = steps                                                    % day loop
            EES.step( pRef( step ), simTime( step ) );                           % sim step
        end % end step
    end % end if-else
    
end % end dayi
end % end function

