
%% Operation strategy 'dynamic feed-in limit'
%
% OSDynamicFeedInLimit function handle for operation strategy 
% 'dynamic feed-in limit' in application to increase self-consumption of 
% system (e.g. residential home storage).
%
% By storing the energy curtailed by a dynamic feed-in limit, curtailment
% losses will be avoided.
% Based on the forecast data the feed-in limit will be adjusted so that the
% storage can be perfectly fully charged with the curtailed energy.
%
% Improvement compared to initial approach by Manuel Pietsch. Energy for
% certain threshold is roughly estimated to obtain near-optimal reference
% feed-in limit. 
% No prediction error correction included yet.
%
% function owner: Nam Truong
% creation date: 10.03.2016
%
%%

function [ EES ] = OSDynamicFeedInLimit( EES, varargin )
% prepare data for OS
sampleTime          = EES.inputSim.sampleTime;
simEnd              = EES.inputSim.simEnd;
simStart            = EES.inputSim.simStart;
stepsPerDay         = 24 * 3600 / sampleTime;
dayStart            = max(simStart/(3600*24),1);
dayEnd              = simEnd/(3600*24);
simTime             = (simStart:sampleTime:simEnd);
simTime             = simTime(2:end);
% assumed system efficiency
assumedSystemEta    = 0.9;

netLoad             = EES.inputProfiles.load - EES.inputProfiles.generation; % residual load from consumption and generated power
forecastNetLoad     = EES.inputForecast.load - EES.inputForecast.generation; % predicted residual load from consumption and generated power

% loop for all days
for dayi = dayStart:dayEnd   % iterate from day EES.simStart to day EES.simEnd
    dayiSteps = (dayi - 1) * stepsPerDay + 1:dayi * stepsPerDay;            % steps from the beginning until the end of dayi
    
    % determine approximate feedInLimit to match storage size
    forecastNetLoadSorted   = - sort( unique ( forecastNetLoad( dayiSteps ) ) );
    powerLimArray         = linspace( forecastNetLoadSorted(1), forecastNetLoadSorted(end), stepsPerDay );
    forecastNetLoadInverted = interp1( forecastNetLoadSorted, 1:length(forecastNetLoadSorted), powerLimArray ) ;
    reqEnergy               = cumsum( forecastNetLoadInverted )* abs( diff( powerLimArray(1:2) ) ) * sampleTime;
    % determine when surplus of generated energy occurs
    stepStartSurplus         = find(forecastNetLoad(dayiSteps) < 0) + dayiSteps(1) - 1;
    if isempty(stepStartSurplus)
        stepStartSurplus = 0;
    end
    % simulate
    for step = dayiSteps 
        % as soon as surplus occurs, calculate remaining storage capacity
        if step == stepStartSurplus(1)
            availEnergy             = 1/assumedSystemEta * ( 1 - EES.SOCNow ) * EES.inputTech.batteryNominalEnergy * EES.SOHCapacityNow;
            [~,initFeedInLimIdx]    = min( (reqEnergy(powerLimArray>=0)-availEnergy).^2 ) ;
            initFeedInLim           = powerLimArray( initFeedInLimIdx + 1 );
            % as no error-correction is included, the initial guess is just used
            % for the feed-in limit control algorithm
            feedInLim = max(0,initFeedInLim,'omitnan');
        end
        % run simulation with determined feedInLimit reference and actual
        % residual load, not prediction
        if netLoad(step) < 0 
            pRef = - min(netLoad(step) + feedInLim, 0);   % power difference between residual load and feedInLimit if residual load exceeds feedInLimit
        else
            pRef = - netLoad(step); % discharge to meet load
        end % end if-else of charging and discharging
        EES.step( pRef, simTime( step ) );                         
    end % end loop for each simulation step
end  % end loop for each day

end


