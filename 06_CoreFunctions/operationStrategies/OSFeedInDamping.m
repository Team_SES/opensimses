%% Operation strategy 'feed-in damping'
%
% OSFeedInDamping function handle for operation strategy
% 'feed-in damping' in application to increase self-consumption of system
% (e.g. residential home storage).
%
% By storing the energy curtailed by the feed-in limit, curtailment losses
% will be avoided.
% By storing with a power depending on the spare charge of the storage and
% the predicted remaining time until sunset the storage will be nearly
% constantly charged over the complete sunshine duration.
% 
% Function implemented according to:
% Zeh, Alexander; Witzmann, Rolf (2014): Operational Strategies for Battery
% Storage Systems in Low-voltage Distribution Grids to Limit the Feed-in
% Power of Roof-mounted Solar Power Systems. In: Energy Procedia 46,
% S. 114�123. DOI: 10.1016/j.egypro.2014.01.164.
%
% function owner: Nam Truong
% creation date: 27.10.2015
% updated: 28.08.2017 by M. F�rstl 
%
%%

function [ EES ] = OSFeedInDamping( EES, varargin )
%% Gather and prepare input data
global gvarDAYS2SECONDS

simStart    = EES.inputSim.simStart;
simEnd      = EES.inputSim.simEnd;
sampleTime  = EES.inputSim.sampleTime;
simTime     = (simStart:sampleTime:simEnd);
simTime 	= simTime(2:end);
stepsPerDay = gvarDAYS2SECONDS / EES.inputSim.sampleTime;
dayStart    = max(simStart/gvarDAYS2SECONDS, 1);
dayEnd      = simEnd / gvarDAYS2SECONDS;

if isempty(EES.powerStorageOp)
    residualLoad         = EES.inputProfiles.load - EES.inputProfiles.generation; % residual load from consumption and generated power
    forecastResidualLoad = EES.inputForecast.load - EES.inputForecast.generation; % predicted residual load from consumption and generated power
else
    residualLoad         = EES.inputProfiles.load + EES.powerStorageOp - EES.inputProfiles.generation; % residual load from consumption and generated power
    forecastResidualLoad = EES.inputForecast.load + EES.powerStorageOp - EES.inputForecast.generation; % predicted residual load from consumption and generated power
end

%% Loop for each day that is simulated
for dayi = dayStart:dayEnd	% iterate from day EES.simStart to day EES.simEnd
    
    stepEnd = (dayi - 1) * stepsPerDay;	% set stepEnd to 0 at the beginning of every day in the for-loop
    
    %% Determine at which simstep sunset occurs
    for step = (dayi * stepsPerDay):-1:((dayi - 1) * stepsPerDay + 1)   % run backwards through all steps of dayi to get timestep of sunset
        if forecastResidualLoad(step) < 0	% if sun is shining
            stepEnd = step;	% timestep where sun sets
            break
        end
    end
    
    %% Loop from beginning of the day until sunset
    if stepEnd ~= (dayi - 1) * stepsPerDay
        
        for step = (dayi - 1) * stepsPerDay + 1:stepEnd
            
            batteryCapacityWithAging = EES.SOHCapacityNow * EES.inputTech.batteryNominalEnergy;
            
            if residualLoad(step) < 0 % more generation than consumption. Battery is charged. Positive values for charging power
                
                if residualLoad(step) < EES.inputTech.power2GridMax(1)	% residualLoad surpasses maximum possible grid feed in
                    curtailment = -residualLoad(step) + EES.inputTech.power2GridMax(1); % this leads to curtailment
                    EES.step(curtailment, simTime(step)); % load battery with curtailment power
                else % if there is no curtailment power
                    chargingPower = (batteryCapacityWithAging - batteryCapacityWithAging * ... % charging power defined by dividing the spare...
                        EES.SOCNow) / ((stepEnd + 1 - step) * EES.inputSim.sampleTime);      % ...charge in the battery with the predicted daytime with radiation (-->s)
                    
                    if chargingPower > -residualLoad(step)% set storage to residualLoad if residualLoad is smaller than charging power...
                        EES.step(-residualLoad(step), simTime(step)); % or discharge if residualLoad is positive
                    else % otherwise charge with chargingpower
                        EES.step(chargingPower, simTime(step));
                    end
                    
                end
                
            else % more consumption than generation. Battery is discharged. Negative values for charging power.
                EES.step(-residualLoad(step), simTime(step));
            end
        end %end of loop until sunset
    end
    
    %% Loop from sunset to end of day
    for step = stepEnd + 1:dayi * stepsPerDay                             % discharge battery after sunset
        EES.step(-residualLoad(step), simTime(step));
    end
end % end loop for each day

end


