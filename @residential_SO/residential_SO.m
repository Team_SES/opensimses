%% Class definition: Residential
%
% Class definition for object with generation, consumption and storage
% device. Based on EESidential consumer with PV-unit and energy storage system.
% Uses system object storage for computation. Additional fields to model
% the system with PV and load. Profiles are included in object.
% Equivalent to building simulink model.
%
% 2017-08-08 Nam Truong
%%

classdef residential_SO < storage_SO
    %%% nontunable properties
    properties (Nontunable, Access = public)
        inputProfiles   = @(x) [];  % load and generation profile stored in object
    end

    %%% externally changeable input
    properties(SetAccess = public)
        inputForecast   = @(x) [];  % forecast of profiles stored in object and changeable
    end
    
    %%% arrays to save history of states etc.
    properties (GetAccess = public, SetAccess = private, Hidden = false)
        powerGrid               % [W] power drawn from grid
        powerCurtail            % [W] curtailed power
        resultsTechnical         % struct with technical results
        inputEconomics          % struct with economic parameters
        resultsEconomics         % struct with economics results
    end

    %%% public methods
    methods
        % constructor
        function EES = residential_SO( varargin )        
            setProperties(EES, nargin, varargin{:});
        end
        
        % methods to change forecast ofter creation of object
        function EES = setLoadForecast( EES, loadFC ) 
            EES.inputForecast.load       = loadFC;   % set new forecast profile
        end
        function EES = setGenerationForecast( EES, genFC ) 
            EES.inputForecast.generation = genFC;    % set new forecast profile
        end
        
        %% Declaration of the methods in separate files     
        [ EES ] = runStorage( EES )
        [ EES ] = evalTechnicalResidential( EES )
        [ EES ] = evalEconomics( EES, varargin )
    end
    
    %%% protected methods
    methods(Access = protected)
        %% Common functions
        function setupImpl(EES)
            setupImpl@storage_SO(EES);   
        end % end of init method

        function loadObjectImpl(EES,s,wasLocked)
            % Set properties in object obj to values in structure s

            % Set private and protected properties
            loadFields = {'powerGrid', ...
                        'powerCurtail', ...
                        'resultsTechnical', ...
                        'inputEconomics', ...
                        'resultsEconomics'};
            for k = 1:numel(loadFields)
                % Load only non discrete object states (DiscreteState are
                % labeld with 'Now' at the end of field name)
                EES.(loadFields{k}) = s.(loadFields{k});
            end  

            % Set public properties and states
            loadObjectImpl@matlab.System(EES,s,wasLocked);
            
            % Select only storage class properties
            s = rmfield(s, loadFields);
            % Set private properties and states of storage object
            loadObjectImpl@storage(EES,s,wasLocked);
        end

        function s = saveObjectImpl(EES)
            % Set properties in structure s to values in object obj

            % Set public properties and states
            s = saveObjectImpl@matlab.System(EES);

           % Set private and protected properties
            saveFields = fieldnames(EES);
            for k = 1:numel(saveFields)
                % Save only non discrete object states (DiscreteState are
                % labeld with 'Now' at the end of field name)
                if(~strcmp(saveFields{k}(end-2:end),'Now'))
                    s.(saveFields{k}) = EES.(saveFields{k});
                end
            end  
        end

    end % methods
end % classdef