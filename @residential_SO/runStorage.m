%% runStorage
% Method of residential class to simulate object in PV home setting. OS is
% predetermined by fhandle during instantiation of class object. 
%
% Input ==
% EES [-] residential class object
%
% Output ==
% EES [-] residential class object
%
% Method is equivalent to Simulink model where the storage system object is
% called each timestep and fed with input power. 
%
% 2017-08-01 Truong
%%

function [ EES ] = runStorage( EES )
    %% run OS (fhandle)
    EES.inputTech.storageOS( EES );
    
    %% calculate relevant power flows
    resLoad     = EES.inputProfiles.load(:) - EES.inputProfiles.generation(:);  % compute residual load
    netLoad     = resLoad + EES.powerStorage(:);                                % compute net load after battery
    pGrid       = max( netLoad, EES.inputTech.power2GridMax(1) );               % compute power drawn from grid (net load after curtailment)
    pCurtail    = pGrid - (netLoad);                                            % compute curtailment
    
    %% write results into object
    EES.powerGrid    = pGrid(:);
    EES.powerCurtail = pCurtail(:);

end

