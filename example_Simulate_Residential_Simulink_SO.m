%% Example simulate residential Simulink
% Example script to start simulation of residential storage. Calling of
% subclass _residential_ for specific application of increasing a
% prosumer's self-consumption.
% 
% Call of simulation with Simulink. Input data is prepared with MATLAB
% script. Check other example simulation script for call of system object 
% in MATLAB environment.
% Warning: By calling the simulation with Simulink, the following functions
% are not applicable: operation strategies, technical and economic
% evaluation, plotting of results
%%
clear
warning('off', 'SystemBlock:MATLABSystem:DiscreteStateNameTruncated')
warning('off', 'SystemBlock:MATLABSystem:ParameterWithUnspecifiedDefaultValueRestrictedToBuiltinDataType')  

%% Preparing MATLAB
% Setting up Matlab search path and global helping variables.

% Global helping variables for conversion
global gvarYEARS2SECONDS gvarDAYS2SECONDS gvarKWH2WS
gvarYEARS2SECONDS   = 3600 * 24 * 365;  % helping variable to convert between years and seconds
gvarDAYS2SECONDS    = 3600 * 24;        % helping variable to convert between days and seconds
gvarKWH2WS          = 3600e3;           % helping variable to convert between kWh and Ws

% Run script to add required subfolders to MATLAB's search path. 
run('addRequiredPaths.m')   

%% Input profiles
% Load chosen profile data into workspace. Used function checks for errors.
% If given load profiles do not exist in folder, please download from
% SimSES Bitbucket download area before or during the function call.
localFilesFolder    = '01_ProfileData';
generationProfile   = returnInputProfile(...
                        'profileFileName',      'PV_EEN_Power_Munich_2014.mat', ...
                        'profileServerPath',    'https://bitbucket.org/Team_SES/opensimses/downloads/', ...
                        'localFilesFolder',     localFilesFolder, ...
                        'verifyHashCode',       false);
                    
loadProfile         = returnInputProfile(...
                        'profileFileName',      'EEN_100_RES_Load_Profiles_1min_1a.mat', ...
                        'profileServerPath',    'https://bitbucket.org/Team_SES/opensimses/downloads/', ...
                        'localFilesFolder',     localFilesFolder, ...
                        'verifyHashCode',       false, ...
                        'profileNumber',        1);
              
%% Configuration of simulation
% Setting of necessary input parameters. Structs hold parameters with
% according fieldnames. Struct fieldnames cannot be changed.
%
% * inputSim
% * inputTech
% * inputEconomics

%% Simulation parameters
% Simulation parameters _inputSim_. Sample time, simulated time and logging
% flags are set here.

inputSim.simStart           = 0 * gvarDAYS2SECONDS;         % [s] starting time of simulation 

inputSim.simEnd             = 1 * gvarYEARS2SECONDS;        % [s] end time of simulation 
inputSim.sampleTime         = 5 * 60;                       % [s] sample time of simulation 

inputSim.loadProfileLength  = 1 * gvarYEARS2SECONDS;        % [s] length of input load profile 
inputSim.genProfileLength   = 1 * gvarYEARS2SECONDS;        % [s] length of input generation profile 

inputSim.plotFrom           = inputSim.simStart;            % [s] starting time of plot 
inputSim.plotTo             = inputSim.simEnd;              % [s] last time to be included in plot 
inputSim.plotTimeUnit       = 'days';                       % [-] depicts time unit for plotting (ticks of x-axis)

inputSim.saveResults        = false;                        % TRUE: the variables will be saved in a .mat-file
inputSim.logBatteryEcOutput = false;                        % TRUE: all output values of equivalent circuit (EC) model are logged
inputSim.logAgingResults    = false;                        % TRUE: all output values of _detectStress_ are logged


%% Technical parameters
% Technical parameters _inputTech_. Check fieldnames of variable to set
% desired desired values.

inputTech = techParamStdPVHomeStorage();                    % generates standard struct for required parameters

%%
% *Modify standard technical parameters:*

%%
% Household configuration is changed from standard case.
inputTech.PVPeakPower           = 10e3;                     % [W] installed capacity of photovoltaic unit
inputTech.batteryNominalEnergy  = 4 * gvarKWH2WS;           % [Ws] single value or array
inputTech.PVCurtailment         = 0.5;                      % [p.u.] max. feed-in power (kW) per kW installed PV capacity

%%
% Choose control strategy / operation strategy (OS). In this case
% conventional greedy strategy for maximum self-consumption rate is
% utilized for simulation.
inputTech.storageOS             = @OSPVHomeGreedy;          % available OS (found in classfolder ('/06_Subfunctions/operationStrategies/')

%%
% Determine aging model and battery type
inputTech.agingModelType        = 'clfp_goebel';                % aging model 
inputTech.batteryType           = 'CLFP_Sony_US26650_Experiment_OCV_R'; % battery type

%% Creating advanced parameter
% Run script that uses pre-determined input parameters to generate more
% complicated input data (price development, efficiency curves, ...)
run('createTechParamPVHomeStorage.m')

%% Simulink parameters
simulinkParam.SaveState                 = 'on';
simulinkParam.SaveOutput                = 'on';
simulinkParam.StateSaveName             = 'stateOut';
simulinkParam.OutputSaveName            = 'yOut';
simulinkParam.MaxStep                   = num2str(inputSim.sampleTime);

%% Generate timeseries out of profiles
simTime             = (inputSim.simStart:inputSim.sampleTime:inputSim.simEnd)';
generationProfile   = timeseries( [0; inputProfiles.generation], simTime );
loadProfile         = timeseries( [0; inputProfiles.load], simTime );
inputPower          = generationProfile - loadProfile;

%% 
% Run simulation with Simulink
disp('Start Simulink Simulation'); 
tic
simOut = sim('sim_storage', simulinkParam);
simulationOutput = simOut.get('simulationOutput');
assignin('base','simulationOutput', simulationOutput);
toc
disp('Simulation complete'); 