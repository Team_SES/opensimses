%% Calculate technics of overall energy storage system
%
% Calculates a struct (resultTech) of the object, holding technical
% evaluations of the operation, losses, energy turnarounds, independent 
% from application.
% 
% Input == 
% EES                           [-]     storage object after simulation
%
% Output ==
% resultTech                    [-]     output struct with technical result fields
%   inStoredenergy              [Ws]    energy charged by BESS
%   outStoredEnergy             [Ws]    energy discharged by BESS
%   lossStorEnergy              [Ws]    losses by energy storage system
%   avgEtaSystem                [pu]    overall system efficiency
%   consumStorageOp             [Ws]    energy for aux components
%   capacityChangeCalendric     [Ws]    capacity change caused by calendric aging
%   capacityChangeCyclic        [Ws]    capacity change caused by cyclical aging
%   capacityChangeTotal         [Ws]    total capacity change
%
% 2017-04-18 Nam Truong 
%
%%

function [ result ] = evalTechnical( EES )
    % EES vars --> local vars
    sampleTime                      = EES.inputSim.sampleTime;                  % sample time
    powerStorage                    = EES.powerStorage;                         % power output of BESS
    
    % in and outward energy of energy storage system
    powerIn                         =   max(powerStorage, 0);                   % charging power
    powerOut                        = - min(powerStorage, 0);                   % discharging power
    inStoredEnergy                  = sum( powerIn  ) * sampleTime;             % energy stored into the storage system [Ws]
    outStoredEnergy                 = sum( powerOut ) * sampleTime;             % energy retrieved from the storage system [Ws]

    % losses and efficiencies
    lossStorEnergy                  = inStoredEnergy - outStoredEnergy;         % losses by energy storage system [Ws]
    avgEtaSystem                    = 1 - lossStorEnergy / inStoredEnergy;      % roundtrip efficiency of storage system
    consumStorageOp                 = sum( EES.powerStorageOp )* sampleTime;    % energy used to operate storage system [Ws]
    
    % calculate resulting aging values
    capacityChangeCalendric         = sum(EES.capacityChangeCalendric);         % capacity change caused by calendric aging
    capacityChangeCyclic            = sum(EES.capacityChangeCyclic);            % capacity change caused by cyclical aging
    capacityChangeTotal             = sum(EES.capacityChangeTotal);             % total capacity change
    
    % Write local vars into output struct
    result.energy___                = [];
    result.inStoredEnergy           = inStoredEnergy;
    result.outStoredEnergy          = outStoredEnergy;
    
    result.efficiency___            = [];
    result.lossStorEnergy           = lossStorEnergy;
    result.avgEtaSystem             = avgEtaSystem;
    result.consumStorageOp          = consumStorageOp;
    
    result.aging___                 = [];
    result.capacityChangeCalendric  = capacityChangeCalendric;
    result.capacityChangeCyclic     = capacityChangeCyclic;
    result.capacityChangeTotal      = capacityChangeTotal;
    
end

