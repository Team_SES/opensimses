%% Function for standard PV-home battery case Germany
% Script to create technical parameters for standard case of residential 
% storage system in Germany for self-consumption increase of households.
%
%   PV kWp          4.4     kW      
%   load            4,500   kWh/a
%   battery size    4.4     kWh
%
%   battery power   4.4     kW
%   PV curtailment  0.7     p.u.
%   PV aging        0.005   p.u./a
%   batt SOC limit  [0, 1]  p.u.
%   batt init SOC   0       p.u.
%   PE efficiency   equation according to SCHMID J., et. al.
%
%   battery aging   
%       measurements        baseLine scenario derived from Rosenkranz with
%                           higher cycle life
%   	stress detection    Half-cycle detection
%       combination         superposition of calendric and cyclical aging
%       init value SOH      0 p.u.
%
% Function to be called in calling script of simulation of residential
% storage systems. Recommendation is to build standard parameterset and
% alter the created values, if necessary.
% Fieldname of struct generated in this function needs to be maintained for
% the object access the correct values.
%
% Nam Truong 2017-08-07
%%

function inputTech = techParamStdPVHomeStorage()
global gvarYEARS2SECONDS gvarDAYS2SECONDS gvarKWH2WS

%% PV system
inputTech.PVPeakPower               = 8.8*1e3;              % [W]
inputTech.PVCurtailment             = 0.4;                  % [pu] ratio of installed PVpeak to be curtailed for feed-in: 0.6 with KfW subsidy, 0.7 in general
inputTech.PVagingPerYear            = 0.5/100;              % [pu] aging per year

%% load
inputTech.annualLoad                = 4500 * gvarKWH2WS;        % [Ws]
inputTech.persistancePeriodLoad     = 0;                    % [s] time period of shifting load profiles (persistant forecast)
inputTech.persistancePeriodPV       = 0;                    % [s] time period of shifting load profiles (persistant forecast)

%% storage
% batteryType:          options for battery types are listed in '/03_SimulationInput/createBatteryData.m'
% agingModelType:       available aging models are listed in '/03_SimulationInput/createAgingModel.m'
% combAgingFct:         functions that combine calendric and cycle aging are located in /03_SimulationInput/combAgingType
% cycleDetectionType:   options for cycle detection method are listed in '/@storage/detectStress.m'  
inputTech.batteryNominalEnergy      = 4.4 * gvarKWH2WS;         % [Ws] single value or array
inputTech.batteryNominalVoltage     = 51.2;                 % [V] desired nominal voltage of battery system --> depending on battery type (EC-model) according # of cells in series with new nominal voltage
inputTech.startSOC                  = 0;                    % [pu] initial SOC at starting step
inputTech.SOCLimLow                 = 0;                    % [pu] lower SOC limit
inputTech.SOCLimHigh                = 1;                    % [pu] upper SOC limit
inputTech.sohCapacityStart          = 1;                    % [pu] initial SOH of storage capacity
inputTech.sohResistanceStart        = 1;                    % [pu] initial SOH of storage resistance
inputTech.batteryType               = 'NMC_Tesla_DailyCyclePowerwall';     % battery type for operation parameters 
inputTech.agingModelType            = 'NMC_Tesla_DailyCyclePowerwall';     % aging model 
inputTech.scheduledNextReplacement  = 0;

% Parameters for stress detection method.
inputTech.stepSizeStressCharacterization    = 1;            % [~] Step size for calling of stress characterization
inputTech.stepSizeCalendarAging             = 1;            % [~] Step size for calling of calendar aging model 
inputTech.stepSizeCycleAging                = 1;            % [~] Step size for calling of cycle aging model (should be smaller or equal to stepSizeStressCharacterization)

% Generate object _stressCharacterization_ to detect the stress inflicted
% on the batteries.
inputTech.callMethodAgingModels     = @callMethodAgingModels_SingleValues;  % Function for the different method and strategies of calling the aging calculation -> see stepsToStartAgingFct

%% Type of thermal model
% TODO: Couple thermal model with selection of setPowerStorage function
inputTech.thermalModelFunction      = @fThermalCell;

%% power electronics
% powerElectronicsMethod:   efficiency curves given in '/03_SimulationInput/createPowerElectronicsData.m'
inputTech.powerElectronicsRatedPower = 4.4 * 1000 ;         % rated power of battery inverter [W]
inputTech.powerElectronicsMethod    = 'Formula';            % expected parameters: 'constant','Formula','Array'
inputTech.powerElectronicsP_0       = 0.0072;               % parameter for 'Formula'
inputTech.powerElectronicsK         = 0.0345;               % parameter for 'Formula'
inputTech.powerElectronicsEta       = 0.95;                 % parameter for 'constant' --> currently not used!

inputTech.etaAccuracy               = 1000;                 % array size for efficiency determination in simulation

%% choose OS
inputTech.storageOS                 = @OSPVHomeGreedy;

% Enviromental parameters
inputTech.temperatureAmbient        = 25 + 273.15;          % [K] temporary constant value
inputTech.powerStorageOp            = 0;                    % [W] power consumption required to operate storage device (sensors, controller, thermal management)     
            
% Get replacement data for setReplacement method
inputTech.replacementData = createBatteryReplacementData('Generic'); % Struct with replacement data

end