%% agingPV
% Function scales given PV profile to consider output reduction due to PV
% cell aging.
%
% PVProfile = agingPV('param', 'value')
%
% Input == (parameters)
% simTime           [s]     simulation time
% PVProfile         [W]     input PV profile to be scaled
% PVagingPerYear    [pu]    aging per year
%
% Output ==
% PVProfile         [W]     Pv profile over the years with aging
%
% PV profile is downscaled for each simulation year for given annuel PV
% aging factor.
%
% 2015-12-01 Moritz Brauchle / Nam Truong
%
%%


function [PVProfile] = agingPV ( varargin )
%% Parsing of input
p = inputParser;    % generate parsing handle
defVal = NaN;       % define default value for missing input
% generate parameters
addParameter(p, 'simTime',          defVal);
addParameter(p, 'PVProfile',        defVal);
addParameter(p, 'PVagingPerYear',   defVal);
% parse input
parse(p, varargin{:})
% write parsed input into local vars
simTime         = p.Results.simTime;
inputPVProfile  = p.Results.PVProfile; 
PVagingPerYear  = p.Results.PVagingPerYear;

%% Scaling of PVprofile to consider reduced output caused by aging
agingFactor     = 1 - PVagingPerYear;                                       % conversion to aging factor
simYears        = simTime / (3600*24*365);                                  % obtaining # of simulation years for scaling
timeVector      = linspace(0, max(1,simYears-1), length(inputPVProfile));   % generate array with year #
pvAging         = agingFactor.^(timeVector);                                % compute array with aging factor for each simulation year
PVProfile       = inputPVProfile.*pvAging(:);                                  % multiply PVprofile with aging factor array to obtain resulting PV profile

end



